/**
 * @Author:R.c
 * @Description: 根据租金排序
 * @Parameter: sort_way 排序方式  houses_sort 排序数据
 * @Date:23:05 2018/4/3
 */
var sortByRentMoney = function (houses_sort,sort_way) {
    if (sort_way==1){
        for(var i=0;i<houses_sort.length-1;i++){
            for(var j=i+1;j<houses_sort.length;j++){
                if(houses_sort[i].rent_money > houses_sort[j].rent_money){//如果前面的数据比后面的大就交换
                    var temp = houses_sort[i];
                    houses_sort[i]=houses_sort[j];
                    houses_sort[j]= temp;
                }
            }
        }
    }else if (sort_way==2){
        for(var i=0;i<houses_sort.length-1;i++){
            for(var j=i+1;j<houses_sort.length;j++){
                if(houses_sort[i].rent_money < houses_sort[j].rent_money){//如果前面的数据比后面的大就交换
                    var temp = houses_sort[i];
                    houses_sort[i]=houses_sort[j];
                    houses_sort[j]= temp;
                }
            }
        }
    }
    return houses_sort;
}
/**
 * @Author:R.c
 * @Description: 根据面积排序
 * @Parameter: sort_way 排序方式  houses_sort 排序数据
 * @Date:23:05 2018/4/3
 */
var sortByArea = function (houses_sort,sort_way) {
    if (sort_way==1){
        for(var i=0;i<houses_sort.length-1;i++){
            for(var j=i+1;j<houses_sort.length;j++){
                if(houses_sort[i].area > houses_sort[j].area){//如果前面的数据比后面的大就交换
                    var temp = houses_sort[i];
                    houses_sort[i]=houses_sort[j];
                    houses_sort[j]= temp;
                }
            }
        }
    }else if (sort_way==2){
        for(var i=0;i<houses_sort.length-1;i++){
            for(var j=i+1;j<houses_sort.length;j++){
                if(houses_sort[i].area < houses_sort[j].area){//如果前面的数据比后面的大就交换
                    var temp = houses_sort[i];
                    houses_sort[i]=houses_sort[j];
                    houses_sort[j]= temp;
                }
            }
        }
    }
    return houses_sort;
}
/**
 * @Author:R.c
 * @Description: 根据居室排序
 * @Parameter: sort_way 排序方式  houses_sort 排序数据
 * @Date:23:05 2018/4/3
 */
var sortByRooms = function (houses_sort,sort_way) {
    if (sort_way==1){
        for(var i=0;i<houses_sort.length-1;i++){
            for(var j=i+1;j<houses_sort.length;j++){
                if(houses_sort[i].rooms > houses_sort[j].rooms){//如果前面的数据比后面的大就交换
                    var temp = houses_sort[i];
                    houses_sort[i]=houses_sort[j];
                    houses_sort[j]= temp;
                }
            }
        }
    }else if (sort_way==2){
        for(var i=0;i<houses_sort.length-1;i++){
            for(var j=i+1;j<houses_sort.length;j++){
                if(houses_sort[i].rooms < houses_sort[j].rooms){//如果前面的数据比后面的大就交换
                    var temp = houses_sort[i];
                    houses_sort[i]=houses_sort[j];
                    houses_sort[j]= temp;
                }
            }
        }
    }
    return houses_sort;
}