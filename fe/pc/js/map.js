//小区数据
var data = '';

//富标注marker
var myRichMarkers = [];
//富标注小区
var myRichMarkersData = [];
//创建小区marker数组
var lastTimeVillageMarker = [];
//创建小区data数组
var lastTimeVillageData = [];
//创建小区最后一次筛选数据，
var lastTimeRenderVillageDate = [];
//所创建的圆的lng
var circle_point_lng ;
//所创建的圆的lat
var circle_point_lat ;
//所创建的圆的标注的lng
var marker_point_lng ;
//所创建的圆的标注的lat
var marker_point_lat ;
//拖动过程中变化的半径
var radius ;
//拖动的米显示label
var label;
// 创建地图实例
var map = new BMap.Map("container-map", {enableMapClick:false});
// 创建点坐标
var point = new BMap.Point(116.404, 39.915);
//聚合标注器
var markerClusterer ;
// 初始化地图，设置中心点坐标和地图级别
var exist = false;
//标注当前图形是圆还是多边形
var isCircleOrPolygon;
//编辑半径
var editData = {};
var editRadius;
var globRadius = 1000;
// 一个诡异需求的开关
var loadingFlag = true;

/***************************************** 王爽变量beigin ***************************************/
//辅助搜索创建的marker
var searchAddMarkers = [];
var word = null;
var local = null;
var objWs;
var lastData;
var wsIndex = undefined;
/***************************************** 王爽变量end ***************************************/

var  final_village_ids;         //小区ID
var  final_city_code;           //城市编码
var  final_data_source;         //房屋类型
var  build_year;                //年代
var  final_rent_way;            //出租方式
var  final_rooms;               //户型
var  final_rent_money;          //租金
var  final_area;                //面积


//=========================  控件  ===========================
var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_LEFT});// 左上角，添加比例尺
// var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
// var bottom_left_navigation = new BMap.NavigationControl({anchor: BMAP_ANCHOR_BOTTOM_LEFT, type: BMAP_NAVIGATION_CONTROL_SMALL}); //左下角，仅包含平移和缩放按钮
var top_right_navigation = new BMap.NavigationControl({anchor: BMAP_ANCHOR_BOTTOM_LEFT, type: BMAP_NAVIGATION_CONTROL_SMALL}); //左下角，仅包含平移和缩放按钮
/******************王国馨begin****************************/
/**
* @Author:王国馨
* @Description:
* @Parameter:
* @Date: 14:10 2018/3/19
*/
//接收username
var strSplit = GetQueryString();
var userName = '';
if (strSplit) {
    userName = strSplit.userName
}
// console.log(userName);
//记录用户选择信息列表或柱状图的状态
var userCheeseStatus = 0;
/***************王国馨end**************************/
/**
 * @Author:R.c
 * @Description: 初始化地图的事件  拖动开始，拖动中，拖动结束等
 * @Parameter:
 * @Date:22:30 2018/2/4
 */
function mapAddEnentListener() {
    map.centerAndZoom(point, 15);               //设置中心点
    map.enableScrollWheelZoom();                //启用滚轮放大缩小
    map.setMapStyle({style:'grayscale'});       //设置地图背景风格
    map.disableDoubleClickZoom();               //禁用双击放大
    map.addEventListener("dragstart",function () {
        hideDiv();
        // console.log("dragstart");
    });
    map.addEventListener("dragging",function () {
        // console.log("dragging");
    });
    map.addEventListener("dragend",function () {
        $('#stretch1').children('img').attr('src', '../images/you.png');
        showDiv();
        // console.log("dragend");
    });
    //单击获取点击的经纬度
    map.addEventListener("click",function(e){
        hideDiv();
        //console.log(e.point.lng + "," + e.point.lat);
    });
    map.addEventListener("zoomend",function () {
        addRichMarker(myRichMarkersData); //添加小区富标注
        //地图缩放到15的时候取消文字标注
        if (map.getZoom() < 15){
            removeRichMarker();
        }
    })
}
/**
* @Author:R.c
* @Description: 实例化百度画图形插件
* @Parameter:
* @Date:13:50 2018/3/21
*/
function setDrawingManager() {
    // 百度地图API功能
    var overlays = [];
    var overlaycomplete = function(e){
        overlays.push(e.overlay);
    };
    var styleOptions = {
        strokeColor:"blue",    //边线颜色。
        fillColor:"blue",      //填充颜色。当参数为空时，圆形将没有填充效果。
        strokeWeight: 2,       //边线的宽度，以像素为单位。
        strokeOpacity: 0.65,    //边线透明度，取值范围0 - 1。
        fillOpacity: 0.1,      //填充的透明度，取值范围0 - 1。
        strokeStyle: 'solid' //边线的样式，solid或dashed。
    }
    //实例化鼠标绘制工具
    var drawingManager = new BMapLib.DrawingManager(map, {
        isOpen: false, //是否开启绘制模式
        enableDrawingTool: true, //是否显示工具栏
        drawingToolOptions: {
            anchor: BMAP_ANCHOR_TOP_LEFT, //位置
            offset: new BMap.Size(161, 0), //偏离值
        },
        circleOptions: styleOptions, //圆的样式
        //polylineOptions: styleOptions, //线的样式
        polygonOptions: styleOptions //多边形的样式
        //rectangleOptions: styleOptions //矩形的样式
    });
    window.drawingManager = drawingManager;
//添加鼠标绘制工具监听事件，用于获取绘制结果
    drawingManager.addEventListener('overlaycomplete', overlaycomplete);

}

/**
* @Author:R.c
* @Description: 编辑圆
* @Parameter:
* @Date:13:25 2018/3/21
*/
function editCircle(){
    removeAll(); //清除上一次的village以及圆
    circleHandle(editData);
}

/**
* @Author:R.c
* @Description:  为创建圆添加事件
* @Parameter:
* @Date:23:56 2018/3/4
*/
function circleHandle(data) {

    editData = data;
    circle_point_lng = data.lng ; //所点击的圆的lng；
    circle_point_lat = data.lat ; //所点击的圆的lat
    editRadius = 0.00001 * globRadius;
    marker_point_lng = circle_point_lng + editRadius ; //所点击的marker的lng
    marker_point_lat = circle_point_lat ;        //所点击的marker的lat

    var circle_point_center = new BMap.Point(circle_point_lng,circle_point_lat);//创建圆心坐标
    var marker_point_radius = new BMap.Point(marker_point_lng,marker_point_lat);//创建半径marker坐标
    console.log(marker_point_radius);

    var radius = getInt(map.getDistance(circle_point_center,marker_point_radius));//得到初始半径
    //console.log(radius);

    createCircle(circle_point_center,radius,marker_point_radius);//创建圆 半径500
    lastTimeVillageMarker = inCircle();  //创建圆内的marker点
    showDiv();
    recoverDefault();   //王爽清空
    map.removeEventListener('click',circleHandle,true);

}

/**
* @Author:R.c
* @Description: 将百度插件的回复为拖动地图
* @Parameter:
* @Date:12:56 2018/3/21
*/
function resetClick() {
    drawingManager.close();
}

/**
 * @Author: W.s.k
 * @Description: 下载数据
 * @Date:15:01 2018/2/2
 */
// $('#downLoadAll').on('click',function () {
//     if( $('#houseItems').find('li').text() == '暂无数据'){
//         test("暂无数据");
//         return;
//     }else{
//         if (loadingFlag) {
//             downloadData();
//             $("#downLoadImg").attr('src', '../images/blueLoading.gif');
//             loadingFlag = false;
//             setTimeout(() => {
//                 $("#downLoadImg").attr('src', '../images/downLoad.png');
//                 loadingFlag = true;
//             }, 3500);
//         }
//     }
// });

/**
 * @Author: S7
 * @Description:  表单获取返回值
 * @Parameter: 
 * @Date: 2018/3/27
 */
 $("#downLoadAll").on('click', function () {
    if( $('#houseItems').find('div').text() == '暂无数据'){
        test("暂无数据");
        return;
    }else{   
        downloadData();
       }
    });

/**
 * @Author: W.s.k & S7
 * @Description: 下载数据
 * @Date:16:00 2018/3/27
 */
function downloadData() {
    if (final_village_ids == undefined || final_city_code == undefined){
        test("没有数据");
        return;
    }

    var form = $("<form>");
    form.attr('id', 'addForm');
    form.attr('style', 'display:none');
    form.attr('target', '');
    form.attr('method', 'post');
    form.attr('action',  base +"/rentmap/writeExcel" );
    var input1 = $('<input>'); input1.attr('type', 'hidden'); input1.attr('name', 'id'); input1.attr('value',  final_village_ids);
    var input2 = $('<input>'); input2.attr('type', 'hidden'); input2.attr('name', 'city_code');  input2.attr('value', final_city_code);
    var input3 = $('<input>'); input3.attr('type', 'hidden'); input3.attr('name', 'data_source'); input3.attr('value',  final_data_source);
    var input4 = $('<input>'); input4.attr('type', 'hidden'); input4.attr('name', 'build_year');  input4.attr('value', final_build_year);
    var input5 = $('<input>'); input5.attr('type', 'hidden'); input5.attr('name', 'rent_way');  input5.attr('value', final_rent_way);
    var input6 = $('<input>'); input6.attr('type', 'hidden'); input6.attr('name', 'rooms'); input6.attr('value',  final_rooms);
    var input7 = $('<input>'); input7.attr('type', 'hidden'); input7.attr('name', 'rent_money');  input7.attr('value', final_rent_money);
    var input8 = $('<input>'); input8.attr('type', 'hidden'); input8.attr('name', 'area'); input8.attr('value',  final_area);
    $('body').append(form);
    form.append(input1);
    form.append(input2);
    form.append(input3);
    form.append(input4);
    form.append(input5);
    form.append(input6);
    form.append(input7);
    form.append(input8);
    // form.submit();
    // form.remove();
    var options  = {    
        url: base +"/rentmap/writeExcel",   //同action 
        type:'post',
        beforeSend:function (xhr) {//请求之前
                $("#downLoadImg").attr('src', '../images/blueLoading.gif');
                form.submit();
          },    
        success:function (data) {   
    // 　　　　　$("#downLoadImg").attr('src', '../images/blueLoading.gif');
        },

    　　 complete:function(xhr){//请求完成
                $("#downLoadImg").attr('src', '../images/downLoad.png');　
                   
               },
        error: function(xhr,status,msg){
                 //alert("状态码"+status+"; "+msg)
                 test('下载数据请求出错!');

              }    
            };  
    $("#addForm").ajaxSubmit(options);
    // console.log(form.submit());
    // console.log($("#addForm").ajaxSubmit(options));
    form.remove();
}

/**
 * @Author: W.s.k
 * @Description:  删除我的筛选条件
 * @Parameter: id: 删除数据的id
 * @Date:15:01 2018/2/3
 */
function deleteVillageSearchHistory(id) {
    deleteVillageSearchHistoryAjax(id,function (result) {
        console.log(result);
        findVillageHistory();
        if(result.code == 200){
            test('删除成功');
        }else{
            test('删除失败');
        }
    });
}
/**
 * @Author: W.s.k
 * @Description: 创建圆心标识
 * @Date:15:01 2018/2/2
 */
function creatCircleCenterMarker(circle_center) {
    var marker_images = new BMap.Icon(
        "../images/circleCenter.png",
        new BMap.Size(17.8,43.3),
        {
//                 offset:new BMap.Size(30,25),
//                imageOffset: new BMap.Size(0, -1),
            imageSize:new BMap.Size(17.8,22.2)
        }
    );
    window.circleCenterMarker = new BMap.Marker(circle_center,{icon:marker_images} ); //创建圆心  更换图片
    map.addOverlay(window.circleCenterMarker);
}
/**
 * @Author: W.s.k
 * @Description: 创建直线
 * @Date:15:01 2018/2/2
 */
function createLineInCircle(startPoint, endPoint) {
    //创建圆心到拖动点之间的直线
    window.circlePolyline = new BMap.Polyline([
        //起点  圆心
        new BMap.Point(startPoint.lng, startPoint.lat),
        //终点 拖动点
        new BMap.Point(endPoint.lng, endPoint.lat)
    ], {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});
    map.addOverlay(window.circlePolyline);

}
/**
 * @Author: W.s.k
 * @Description: 编辑圆心到拖动点之间的直线
 * @Date:15:01 2018/2/2
 */
function editeLineInCircle(startPoint, endPoint) {
    //设置直线第二个点（拖动点）的位置
    window.circlePolyline.setPositionAt(1, endPoint);
    map.addOverlay(window.circlePolyline);
}

/**
 * @Author:R.c
 * @Description: 清楚地图上的图形绘制
 * @Date:11:07 2018/1/24
 */
function removeAll() {
    removeSearchMarkers(searchAddMarkers); //清除搜索后产生的marker点
    hideDiv();
    removeCircile();          //移除圆相关
    removePolygon();          //清除多边形的数据
    removeAllVillageMarker(); //移除圆内所有的小区
    removeRichMarker();       //移除圆内所有的小区描述
    clearInput(); //清除页面中的输入框
}
/**
* @Author:R.c
* @Description: 此方法为 暴露一个清除圆及其圆相关的控件的方法
* @Date:15:44 2018/3/4
*/
function removeCircile() {
    window.circle && map.removeOverlay(window.circle);                  //判断圆是否存在
    exist = false;                                                      //标记圆不存在
    window.marker && map.removeOverlay(window.marker);                   //移除拖动点
    window.circleCenterMarker && map.removeOverlay(window.circleCenterMarker);    //移除圆心
    window.circlePolyline && map.removeOverlay(window.circlePolyline);   //移除圆心到拖动点的距离
    map.removeEventListener('click',circleHandle,true);                 //移除创建圆的点击事件
}
/**
 * @Author:R.c
 * @Description: 初始化方法，为MarkerClusterer赋值
 * @Parameter:
 * @Date:18:21 2018/3/20
 */
function setMarkerClusterer() {

    if ( markerClusterer == undefined ){
        markerClusterer = new BMapLib.MarkerClusterer(map,{maxZoom:14});
    }

}
/**
 * @Author:R.c
 * @Description: 通过百度插件创建圆
 * @Parameter:
 * @Date:12:42 2018/3/21
 */
function createCircleByBaidu(circle,centerPoint) {
    editData = centerPoint;
    window.circle = circle;
    creatCircleCenterMarker(centerPoint);
    exist = true;                              //标记圆存在
    isCircleOrPolygon = 0;                      //标记当前图形是圆
}
/**
 * @Author:R.c
 * @Description: 创建一个圆上的标注
 * @Parameter:
 * @Date:9:25 2018/1/24
 */
function createMarker(circle_center,marker) {
    var marker_images = new BMap.Icon(
        "../images/dragIcon.png",
        new BMap.Size(12,12),
        {
//               offset:new BMap.Size(60,25),
//               imageOffset: new BMap.Size(0, -1),
            imageSize:new BMap.Size(12,12)
        }
    );
    window.marker = new BMap.Marker(marker,{icon:marker_images}); //创建按钮拖动小组件
    window.marker.enableDragging();

    var preRadius = map.getDistance(circle_center,marker);        //获取半径 进行校准
    var labelText = '<span style="background: #efefef;padding: 4px">'+getInt(preRadius)+'</span>';

    label = new BMap.Label( labelText +" 米",{offset:new BMap.Size(30, -8)});
    label.setStyle({ color : "black",borderColor : "white",fontSize : "12px" ,backgroundColor:"white",boxShadow: "1px 1px 5px #aaa",padding: "4px 6px",borderRadius: "3px"});     //设置拖动点标签样式
    window.marker.setLabel(label);

    createLineInCircle(circle_center,marker);//创建圆心到拖动点的直线
    //新增可输入半径yf
    label.addEventListener("click", EditorMarker);     //给label添加点击事件

    window.marker.addEventListener("dragstart",function () {
        enableMap();
        clearInput();
        hideDiv();
        removeAllVillageMarker();//移除圆内所有小区
        removeRichMarker();//移除圆内所有小区富标注
        recoverDefault();//清楚选中的东西
    });
    window.marker.addEventListener("dragging",function (e) {
        radius = getInt(map.getDistance(circle_center,e.point));
        againRender(radius,marker);                         //渲染圆
    });
    window.marker.addEventListener("dragend",function () {
        $('#stretch1').children('img').attr('src', '../images/you.png');
        dialogShow();
        radius = getInt((map.getDistance(circle_center,window.marker.getPosition())));
        marker_point_lng = window.marker.getPosition().lng;
        marker_point_lat = window.marker.getPosition().lat;
        lastTimeVillageMarker = inCircle();
        showDiv();
        backWs();//拖动结束,相当于点击返回按钮
        NotypeBtn();
    });
    map.addOverlay(window.marker);
}
/**
 * @Author: R.c
 * @Description: 1.渲染圆的半径 2.圆心到标志的直线 3.显示的米
 * @Parameter: radius：半径  circle_point_center：圆心
 * @Date:2:22 2018/2/5
 */
function againRender(radius,circle_point_center) {
    window.circle.setRadius( radius );
    var labelText = '<span style="background: #efefef;padding: 4px">'+radius+'</span>';
    label.setStyle({ color : "black",borderColor : "white",fontSize : "12px" ,backgroundColor:"white",boxShadow: "1px 1px 5px #aaa",padding: "4px 6px",borderRadius: "3px"});     //设置拖动点标签样式
    window.label.setContent( labelText +" 米");
    editeLineInCircle(circle_point_center,window.marker.getPosition());  //编辑圆心与拖动点之间的直线 重置距离
}

/**
 * @Author:R.c
 * @Description: 异常圆内所有小区
 * @Date:13:02 2018/2/3
 */
function removeAllVillageMarker() {
    if(lastTimeVillageMarker.length){
        if(lastTimeVillageMarker.length){
            markerClusterer.removeMarkers(lastTimeVillageMarker);
        }
    }
}

/**
 * @Author:R.c
 * @Description: 创建一个圆
 * @Parameter: circle_point_center 圆心   r:半径
 * @Date: 19:48 2018/1/22
 */
function createCircle(circle_point_center, radius_p,marker_point_radius) {

    window.circle = new BMap.Circle(circle_point_center, radius_p , {
        strokeColor: "blue",
        strokeWeight: 2,
        fillColor: "blue",
        fillOpacity: 0.1,
        strokeOpacity: 0.65
    });
    map.centerAndZoom(circle_point_center, 15);//把光标移动圆心
    radius = getnum(radius_p);                 //将半径值赋给成员变量
    map.addOverlay(window.circle);             //增加圆
    exist = true;                              //标记圆存在
    isCircleOrPolygon = 0;                      //标记当前图形是圆

    creatCircleCenterMarker(circle_point_center);  //创建圆心
    createMarker( circle_point_center,marker_point_radius);         //创建拖动标注

}

/**
 * @Author: R.c
 * @Description: 判断经纬度是否在圆内
 * @Parameter:
 * @Date: 10:42 2018/1/17
 */
function inCircle() {
    //计算半径
    if(data == '' ){
        getVillage(undefined,getCity(),undefined,undefined,undefined,undefined,undefined,undefined,function (result) {
            data = result;
        });
    }
    var points = [];
    var markers = [];
    var isExistVillage = [];
    lastTimeVillageData = [];
    if (data != null && data.code == 200){
        var villages = data.data;
        var point = '';
        for(var i =0;i<villages.length;i++) {
            point = new BMap.Point( getnum(villages[i].longitude), getnum(villages[i].latitude) );
            if ( BMapLib.GeoUtils.isPointInCircle(point, window.circle) ) {
                points[i] = point;
                isExistVillage.push(villages[i]);
            }
        }
        getVillage(getIds(isExistVillage),getCity(),undefined,undefined,undefined,undefined,undefined,undefined,function (result) {
            if (result.code == 200){
                lastTimeVillageData = result.data;
                lastTimeRenderVillageDate = result.data;
                lastData = lastTimeRenderVillageDate;
                //根据用户选择渲染
                userCheese();
                setDownloadData();
                myRichMarkersData = lastTimeVillageData;
                addRichMarker(myRichMarkersData);
                dialogHide();
            } else if(result.code == 201){
                console.log('没数据');
                removeEchart();//清除Echarts数据
                dialogHide();
                removeRightHouse();
            }else {
                console.log("inCircle 出错");
            }

        });
        markers = addValidMarkers(points);
    }
    return markers;
}
/**
* @Author:R.c
* @Description: 拼接id
* @Parameter: 需要拼接的参数
* @Date:1:01 2018/3/2
*/
function getIds(data) {
    var id = "";
    for(var i = 0;i <= data.length - 1;i++){
        id += data[i].id + ","
    }
    id = id.substr(0,id.length-1);//去掉最后一个空格
    id = isList(id);//判断长度是否大于0，否则赋值为 undefined
    return id;
}
/**
 * @Author:R.c
 * @Description: 给下载数据赋值
 * @Date:17:33 2018/2/2
 */
function setDownloadData() {
    var id = '';
    if(lastTimeVillageData.length>0){
        for(var i = 0;i <= lastTimeVillageData.length - 1;i++){
            id += lastTimeVillageData[i].id + ","
        }
        id = id.substr(0,id.length-1);//去掉最后一个空格
        id = isList(id);//判断长度是否大于0，否则赋值为 undefined
        final_village_ids = id;
    }else{
        id = undefined;
    }
    //未来从标签中拿
    final_city_code = getCity();                             //城市编码
    final_data_source = undefined;                           //房屋类型
    final_build_year = undefined;                            //年代
    final_rent_way = undefined;                              //出租方式
    final_rooms = undefined;                                 //户型
    final_rent_money = undefined;                            //租金
    final_area = undefined;                                  //面积

}
/**
* @Author:R.c
* @Description: 重置下载数据
* @Parameter:
* @Date:16:01 2018/3/2
*/
function reSetDownloadData() {
    final_village_ids = undefined ;         //小区ID
    final_city_code = undefined ;           //城市编码
    final_data_source = undefined ;                      //房屋类型
    final_build_year = undefined;                            //年代
    final_rent_way = undefined ;                         //出租方式
    final_rooms = undefined ;                            //户型
    final_rent_money = undefined ;                       //租金
    final_area = undefined ;                             //面积
}

/**
 * @Author:R.c
 * @Description: 筛选小区的方法
 * @Parameter:
 * @Date:19:16 2018/1/29
 */
function screenVillage(parameters) {

    console.log("screenVillage!!");

    var id ="" ;        //圆内的所有小区
    var city_code ;     //城市
    var data_source;    //数据源
    var build_year;     //年代
    var rent_way;       //租赁方式
    var rooms;          //居室
    var rent_money;     //租金
    var area;           //面积
    //给id赋值

    if(lastTimeVillageData.length>0){
        for(var i = 0;i <= lastTimeVillageData.length - 1;i++){
            id += lastTimeVillageData[i].id + ",";
        }
        id = id.substr(0,id.length-1);//去掉最后一个空格
        id = isList(id);//判断长度是否大于0，否则赋值为 undefined
        final_village_ids = id;
    }else{
        dialogHide();
        test("暂无数据");
        return;
        //id = undefined;
    }
    //注意，当从标签中拿到的数据为空或者默认时，请将值赋为 undefined
    //给city_code赋值
    city_code = parameters[0];
    final_city_code = city_code ;
    //给data_source赋值
    data_source = parameters[1];
    final_data_source = data_source;
    //给year序号赋值
    build_year = parameters[2];
    final_build_year = build_year;
    //给rent_way赋值
    rent_way = parameters[3];
    final_rent_way = rent_way;
    //给rooms赋值
    rooms = parameters[4];
    final_rooms = rooms;
    //给rent_money赋值
    rent_money = parameters[5];
    final_rent_money = rent_money;
    //给area赋值
    area = parameters[6];
    final_area = area;


    screenVillageAjaxAndRender(id ,city_code,data_source,build_year,rent_way,rooms,rent_money,area);

}
/**
* @Author:R.c
* @Description: 渲染小区的调用以及动画效果
* @Parameter:
* @Date:17:34 2018/2/23
*/
function screenVillageAjaxAndRender(id ,city_code,data_source,build_year,rent_way,rooms,rent_money,area) {
    //发送ajax查询数据
    loading();
    getVillage(id,city_code,data_source,build_year,rent_way,rooms,rent_money,area,function (result) {
        var screenVillage = result;
        lastTimeRenderVillageDate = result;
        lastData = lastTimeRenderVillageDate;
        console.log("screenVillage拿到的数据:");
        console.log(screenVillage);
        //给最后一次数据赋值
        if (screenVillage.code == 200){
            myRichMarkersData = screenVillage.data;
        }else {
            myRichMarkersData = [];
            lastTimeRenderVillageDate = [];
            lastData = lastTimeRenderVillageDate;
        }
        //根据用户选择渲染
        userCheese();
        //添加小区富标注
        addRichMarker(myRichMarkersData);
        //调用渲染圆内小区方法（圆内筛选过的和没有筛选出去的）
        lastTimeVillageMarker = screenVillageRender(screenVillage);
    });
}
/**
 * @Author:R.c
 * @Description: 渲染圆内小区，（圆内筛选过的和没有筛选出去的）
 * @Parameter:  screenVillage：筛选出来的小区数据
 * @Date:9:48 2018/2/6
 */
function screenVillageRender(screenVillage) {
    //计算半径
    removeAllVillageMarker();
    var points = [];
    var markers = [];
    if (screenVillage != null && screenVillage.code == 200){
        var villages = screenVillage.data;
        var point = '';
        for(var i =0;i<villages.length;i++) {
            point = new BMap.Point( getnum(villages[i].longitude), getnum(villages[i].latitude) );
            if (isCircleOrPolygon == 0){
                if ( BMapLib.GeoUtils.isPointInCircle(point, window.circle) ) {
                    points[i] = point;
                }
            }else {
                if ( BMapLib.GeoUtils.isPointInPolygon(point, window.markersAll.polygon) ) {
                    points.push(point);
                }
            }
        }
        markers = addValidMarkers(points);
    }
    //模态框消失
    dialogHide();
    return markers;
}

/**
 * @Author:R.c
 * @Description: 通过筛选条件查询小区
 * @Date:17:29 2018/2/1
 */
function findByScreeningConditions(id ,city_code,data_source,build_year,rent_way,rooms,rent_money,area,circle_center,circle_radius,circle_marker,polygon_points) {
    removeAll();
    if(polygon_points == undefined){
        //筛选条件为圆
        var cirlce_center_string = circle_center.split(",");
        var circle_marker_string = circle_marker.split(",");
        createCircle(new BMap.Point(cirlce_center_string[0], cirlce_center_string[1]),circle_radius,new BMap.Point(circle_marker_string[0], circle_marker_string[1]));
    }else {
        //筛选条件为多边形
        createPolygonBySearchVillage(polygon_points);
    }
    screenVillageAjaxAndRender(id ,city_code,data_source,build_year,rent_way,rooms,rent_money,area);
}

/**
 * @Author:R.c
 * @Description: 搜索 通过搜索内容
 * @Parameter: searchInput：搜索内容  user：用户
 * @Date:0:33 2018/2/5
 */
function findVillageSearchHistoryByLuence() {

    var searchInput = $('#inputCondition').val();
    var user = userName;
    var city_code = getCity();
    if(searchInput==""){
        findVillageHistory();
    }else {
        //开始搜索
        findVillageSearchHistoryByLuenceAjax(searchInput,user,city_code,function (result) {
            //渲染筛选数据
            if(result){
                if(result.code == 200){
                    var resultData = result.data;
                    villageHistoryRenderHtml(resultData);
                }else{
                    console.log('暂无数据');
                }
            }
        });

    }
}

/**
 * @Author:R.c
 * @Description: 保存小区筛选数据
 * @Parameter: user:用户id , name:所保存的筛选条件名,village_ids:小区数组,city_code：城市编号, data_source：数据来源, rent_way：出租方式,
 *               rooms:户型,  rent_money：租金, area：面积, circle_center：圆心, circle_radius：圆半径
 * @Date:13:15 2018/2/1
 */
function saveVillageHistory(data,conditionName) {

    console.log("saveVillageHistory");
    console.log(data);

    var user = undefined;           //用户id
    var name = undefined;           //所保存的筛选条件名
    var village_ids = undefined;    //小区数组
    var city_code = undefined;      //城市编号
    var data_source = undefined;    //数据来源
    var build_year = undefined; //年代范围
    var rent_way = undefined;       //出租方式
    var rooms = undefined;     //户型
    var rent_money = undefined;     //租金
    var area = undefined;           //面积
    var circle_center = undefined;  //圆心
    var circle_marker = undefined;  //圆上的标注
    var circle_radius = undefined;  //圆半径
    var polygon_points = undefined; //多边形的坐标点

    user = userName;
    name = conditionName;
    //给id赋值
    village_ids = '';
    if(lastTimeVillageData.length>0) {
        for (var i = 0; i <= lastTimeVillageData.length - 1; i++) {
            village_ids += lastTimeVillageData[i].id + ","
        }
        village_ids = village_ids.substr(0,village_ids.length-1);//去掉最后一个空格
        village_ids = isList(village_ids);//判断长度是否大于0，否则赋值为 undefined
    }else{
        village_ids = undefined;
    }

    city_code = data[0];
    data_source = data[1];
    build_year = data[2];
    rent_way = data[3];
    rooms = data[4];
    rent_money = data[5];
    area = data[6];

    if (isCircleOrPolygon == 0){
        circle_center = circle_point_lng + "," + circle_point_lat;
        circle_marker = marker_point_lng + "," + marker_point_lat;
        circle_radius = radius ;
    }

    if (isCircleOrPolygon == 1){
        polygon_points = getPolygonPoint();
    }

    saveVillageSearchHistory(user,name,village_ids , city_code , data_source , build_year, rent_way,
        rooms, rent_money, area, circle_center, circle_marker,circle_radius,polygon_points,function (result) {
            if(result){
                if(result.code == 200){
                    test('保存成功');
                }else if(result.code == 300) {
                    test('保存失败,筛选条件名已存在');
                }else if(result.code == 500) {
                    test(result.msg);
                }else {
                    test('保存失败');
                }
            }
        });

}

/**
 * @Author:R.c
 * @Description: 显示两侧 隐藏百度地图控件
 * @Parameter:
 * @Date:16:25 2018/2/5
 */
function showDiv() {
    if (isExist()){
        showRighOrDown('#mainMenuRight','right','0');
    }
    //showRighOrDown('#showOrHide','top','0');
    //delete_control();
}

/**
 * @Author:R.c
 * @Description: 隐藏两侧 显示百度地图控件
 * @Parameter:
 * @Date:16:25 2018/2/5
 */
function hideDiv() {
    hideRightOrUp('#mainMenuRight','right','-430px');
}

/**
 * @Author:R.c
 * @Description:  查询该用户下的历史筛选条件
 * @Parameter: user：用户id
 * @Date:14:12 2018/2/1
 */
function findVillageHistory() {
    var user ;  //用户id
    var city_code ;  //城市
    user = userName;
    city_code = getCity();
    findVillageSearchHistory( user ,city_code , function (result) {
        //调用渲染历史筛选条件方法
        villageHistoryRender(result);
    } );

}

/**
 * @Author:R.c
 * @Description: 数据校验
 * @Parameter: list 需要校验的数组
 * @Date:19:54 2018/1/29
 */
function isList(list) {
    if(list.length > 0){
        return list;
    }else {
        return undefined;
    }
}
/**
 * @Author:R.c
 * @Description: 判断是否画圆
 * @Date:14:17 2018/2/2
 */
function isExist() {
    return exist;
}

/**
 * @Author:R.c
 * @Description: 添加控件和比例尺
 * @Date:13:59 2018/2/3
 */
function add_control(){
    map.addControl(top_left_control);
    //map.addControl(top_left_navigation);
    map.addControl(top_right_navigation);

    $(".BMap_stdMpType1").css({"bottom":"50px","left":"20px"});
    $(".BMap_scaleCtrl").css({"bottom":"25px","left":"100px"});

}
/**
 * @Author:R.c
 * @Description: 移除控件和比例尺
 * @Date:13:59 2018/2/3
 */
function delete_control(){
    map.removeControl(top_left_control);
    //map.removeControl(top_left_navigation);
    map.removeControl(top_right_navigation);
}

/**
 * @Author:R.c
 * @Description: 编写自定义函数,创建有效标注
 * @Parameter: point 坐标点
 * @Date:13:59 2018/2/3
 */
function addValidMarkers(points){
    var markers = [];
    var marker_images = new BMap.Icon(
        "../images/blueMaker.png",
        new BMap.Size(17.8,43.3),
        {
            //offset:new BMap.Size(20,20),
            //imageOffset: new BMap.Size(0, -10),
            imageSize:new BMap.Size(17.8,22.2)
        }
    );
    //聚合标注器
    console.log("markerClusterer");
    console.log(markerClusterer);
    for (var i = 0;i<points.length;i++){
        markers[i] = new BMap.Marker(points[i],{icon:marker_images} );
    }
    setMarkerClusterer();
    markerClusterer.addMarkers(markers);
    return markers;
}
/**
* @Author:R.c
* @Description: 为小区marker添加富标注
* @Parameter: marker 当前小区marker对象
* @Date: 14:30 2018/2/26
*/
function addRichMarker(data) {
    if (map.getZoom()<15){   //如果地图缩放等级小于15 不添加
        return;
    }
    removeRichMarker();
    var villages = data;
    myRichMarkers = [];
    for (var i = 0;i<data.length;i++){
        var html2 = '<div style="position: absolute; margin: 0pt; padding: 0pt; width: 80px; height: 26px; left: -10px; top: -35px;">'
            +     '<span id="rm3_image'+i+'" class="redBorder" style="border:none;left:5px; top:0px; position:absolute;width:25px;height:22px;background: #4C98FF;color:#fff; text-align:center;">'+i
            +     '<span style="position:absolute;top:22px;left:7px;width: 0;height:0;border:6px solid rgba(0,0,0,0);border-top-color: #4C98FF;"></span>'
            +     '</span>'
            + '</div>'
            + '<label class=" BMapLabel rm3_image'+i+'" unselectable="on" style="background: #4C98FF;position: absolute; -moz-user-select: none; height:22px; display: inline; cursor: inherit; border: 0px none; padding: 2px 5px 1px; white-space: nowrap; font: 12px arial,simsun; z-index: 80; color: #fff; left: -6px; top: -35px;border-radius: 2px;">'+villages[i].name+' '+villages[i].houses.length+'套'+'</label>',
            myRichMarker = new BMapLib.RichMarker(html2,  new BMap.Point(getnum(villages[i].longitude), getnum(villages[i].latitude)),{
                "anchor" : new BMap.Size(-7, -15),
                "enableDragging" : false });
        myRichMarkers.push(myRichMarker) ;
        map.addOverlay(myRichMarker);

        // myRichMarker.addEventListener("onmouseover", function(e) {
        //     var id = this._container.children[0].children[0].id;
        //     document.getElementById(this._container.children[0].children[0].id).style.background = 'red';
        //     $('#'+id).children('span').css('border-top-color','red');
        // });
        // myRichMarker.addEventListener("onmouseout", function(e) {
        //     var id = this._container.children[0].children[0].id;
        //     document.getElementById(id).style.background = 'rgb(255, 102, 0)';
        //     $('#'+id).children('span').css('border-top-color','rgb(255, 102, 0)');
        // });
        myRichMarker.addEventListener("click", function(e) {
            var id = this._container.children[0].children[0].id;
            document.getElementById(id).style.background = '#E66C66';
            $('span').removeClass('redBorder');
            $('.BMapLabel').removeClass('redActive');
            $('.'+id).parent().siblings().removeClass('addzindex');
            $('#'+id).children('span').addClass('redBorder');
            $('.'+id).addClass('redActive');
            $('#back').show();
            //层级修改
            var getZindex = $('.'+id).parent().css('z-index');
            $('.'+id).parent().addClass('addzindex');
            var id = this._container.children[0].children[0].id;
            var index = id.substring(9);
            objWs = {};
            objWs.data = [];
            objWs.data[0] = villages[index];
            objWs.code = 200;
            final_village_ids = villages[index].id;
            rightHouse(objWs, index);
            getEchartData(objWs);
        });

    }
}
/**
* @Author:R.c
* @Description:
* @Parameter:
* @Date:15:14 2018/2/26
*/
function removeRichMarker() {
    console.log("come removeRichMarker");
    console.log(myRichMarkers);
    for (var i=0;i<myRichMarkers.length;i++){
        map.removeOverlay(myRichMarkers[i]);
    }

}


/**
 * @Author:R.c
 * @Description: 编写自定义函数,创建失效标注
 * @Parameter: point 坐标点
 * @Date:13:59 2018/2/3
 */
function addLoseMarker(point){
    var marker_images = new BMap.Icon(
        "../images/loseMarker.png",
        new BMap.Size(17.8,43.3),
        {
            //offset:new BMap.Size(20,20),
            //imageOffset: new BMap.Size(0, -10),
            imageSize:new BMap.Size(17.8,22.2)
        }
    );
    var marker = new BMap.Marker(point,{icon:marker_images} );
    map.addOverlay(marker);
    return marker;
}


/**
 * @Author:R.c
 * @Description: 保留小数点前6位
 * @Parameter: num : 需要处理的数据
 * @Date:13:58 2018/2/3
 */
function getnum(num)
{
    return parseFloat(num).toFixed(6);
}
/**
 * @Author:R.c
 * @Description: 保留小数点前2位
 * @Parameter: num : 需要处理的数据
 * @Date:13:58 2018/2/3
 */
function getInt(num)
{
    return parseFloat(num).toFixed(2);
}
/**
* @Author:WangShuang
* @Description: 只要整数
* @Parameter:
* @Date: 上午10:13 18/3/26
*/
function getInt(num){
    return parseInt(num);
}
/**
* @Author:R.c
* @Description: 清空房源右侧数据
* @Date:19:36 2018/2/6
*/
function removeRightHouse() {
    $('#houseItems').html('<div style="text-align: center;color: #ccc;border: none;margin-top: 10px;">暂无数据</div>');
    $('#paging').html('');
    $('#paging').hide();
    $('#houseNum').text(0);
}
var citydata = [
    {cityCode:110100,cityName:'北京市'},
    {cityCode:500100,cityName:'重庆市'},
    {cityCode:430100,cityName:'长沙市'},
    {cityCode:620000,cityName:'东莞市'},
    {cityCode:210200,cityName:'大连市'},
    {cityCode:330100,cityName:'杭州市'},
    {cityCode:440100,cityName:'广州市'},
    {cityCode:340100,cityName:'合肥市'},
    {cityCode:370100,cityName:'济南市'},
    {cityCode:320100,cityName:'南京市'},
    {cityCode:370200,cityName:'青岛市'},
    {cityCode:310100,cityName:'上海市'},
    {cityCode:320500,cityName:'苏州市'},
    {cityCode:440300,cityName:'深圳市'},
    {cityCode:350200,cityName:'厦门市'},
    {cityCode:130100,cityName:'石家庄市'},
    {cityCode:420100,cityName:'武汉市'},
    {cityCode:320200,cityName:'无锡市'},
    {cityCode:610100,cityName:'西安市'},
    {cityCode:410100,cityName:'郑州市'}
];
/**
 * @Author:WangShuang
 * @Description:渲染城市
 * @Parameter:
 * @Date: 下午3:13 18/3/12
 */
function renderCity(citydata){

    var str = '';
    for(var i=0;i<citydata.length;i++){
        str +='<li data-code="'+citydata[i].cityCode+'">'+citydata[i].cityName+'</li>'
    }
    $('.cityUl').html(str);
}
/**
* @Author:WangShuang
* @Description:搜索城市 前端模拟模糊搜索
* @Parameter:
* @Date: 下午5:01 18/3/12
*/
function resultArr(citydata,cityValue) {
    var arr = [];
    for(var x=0;x<citydata.length;x++){
        if(citydata[x].cityName.indexOf(cityValue)>=0) {
            arr.push(citydata[x]);
        }
    }

    return arr;
}
/**
* @Author:WangShuang
* @Description:切换城市
* @Parameter:
* @Date: 下午5:02 18/3/12
*/
function changeCity(citydata){
    var num = 1;
    $('#city').click(function(){
        num++;
        if(num%2==0){
            renderCity(citydata);
            $('#city').css('border-bottom','1px solid #efefef');
            $('.city').show();
        }else{
            $('#city input').val($('#city input').attr('data-cityName')).blur();
            $('.city').hide();
        }

    });


    $('#city input').on('keyup',function(){
        var liStr = '';
        var cityValue = $('#city input').val();
        var arr = [];
        arr = resultArr(citydata,cityValue);
        if(arr.length > 0){
            for(var i=0;i<arr.length;i++){
                liStr += '<li data-code="'+arr[i].cityCode+'">'+arr[i].cityName+'</li>';

            }
        }else{
            if($(this.searchInput).val() != ""){
                liStr += '<p style="padding: 14px;color:#666;">无搜索结果</p >';
            }else{
                renderCity(citydata);
            }

        }

        $('.cityUl').html(liStr);

    });
    $('.city').on('click','li' ,function(){
        hideDiv();//右侧隐藏
        $('#city input').val($(this).text());
        $('#city input').attr('data-code',$(this).attr('data-code'));
        $('#city input').attr('data-cityName',$(this).text());
        getVillageFirst(getCity(),function (result) {
            data = result;
        });
        num=1;
        //清理所有的圆或点
        removeAll();
        //清空右侧的数据列表
        removeRightHouse();
        //切换成当前选中的城市
        map.centerAndZoom($(this).text(), 15);
        $('.city').hide();
        switchCity();      //初始化城市
    });
    // $('.mainMenuAdress').mouseleave(function(){
    //     $('.city').hide();
    // });

}

/**
 * @Author: 杨帆
 * @Description:  切换城市之前清空数据
 * @Parameter:
 * @Date: 17:30 2018\3\8
 */
function switchCity() {
    $('#circle_map').removeClass('active');
    $('#polygon_map').removeClass('active');
}

/**********************************************  王爽begin  ***************************************************8*/
/**
 * @Author:WangShuang
 * @Description: 辅助搜索创建新的标注
 * @Parameter:
 * @Date: 下午3:04 18/2/28
 */
function searchAddMaker(arr){

    var marker_image = new BMap.Icon(
        "../images/blueMaker.png",
        new BMap.Size(20.8,43.3),
        {
            imageSize:new BMap.Size(17.8,21.2)
        }
    );
    for(var i=0;i<arr.length;i++){
        var markerSearch = new BMap.Marker(new BMap.Point(arr[i].lng, arr[i].lat),{icon:marker_image});
        searchAddMarkers.push(markerSearch);
        markerSearch.disableDragging();
        var label = new BMap.Label((i+1),{offset:new BMap.Size(4.2,1)});
        label.setStyle({
            background:'none',color:'#fff',border:'none'
        });
        markerSearch.setLabel(label);
        markerSearch.addEventListener("onmouseover", function(){

            this.setIcon(new BMap.Icon(
                "../images/redMaker.png",
                new BMap.Size(20.8,43.3),
                {
                    imageSize:new BMap.Size(17.8,21.2)
                }));
        });
        markerSearch.addEventListener("onmouseout", function(){

            this.setIcon(new BMap.Icon(
                "../images/blueMaker.png",
                new BMap.Size(20.8,43.3),
                {
                    imageSize:new BMap.Size(17.8,21.2)
                }));
        });
        markerSearch.addEventListener("click",function(){
            var p = this.getPosition();  //获取marker的位置

            for(var j=0;j<arr.length;j++){
                if(p.lng == arr[j].lng && p.lat == arr[j].lat){
                    var title = arr[j].title,address = arr[j].address;
                }
            }
            map.setCenter(new BMap.Point(p.lng, p.lat));
            map.panTo(new BMap.Point(p.lng, p.lat));
            var sContent ='<p class="adressTitle">'+title+'</p>' +
                '<p class="adressDetail">'+address+'</p>';


            var opts = {
                enableMessage: false,
                offset:new BMap.Size(0,-20)
            };
            var infoWindow = new BMap.InfoWindow(sContent, opts);
            this.openInfoWindow(infoWindow);
        });
        map.setCenter(new BMap.Point(arr[0].lng, arr[0].lat));
        map.panTo(new BMap.Point(arr[0].lng, arr[0].lat));
        map.addOverlay(markerSearch);    //增加点

    }

}
/**
 * @Author:WangShuang
 * @Description: 清空辅助搜索marker
 * @Parameter:
 * @Date: 上午10:45 18/3/2
 */
function removeSearchMarkers(arr){
    for (var i=0;i<arr.length;i++){
        map.removeOverlay(arr[i]);
    }
    map.closeInfoWindow();
}
/**
 * @Author:WangShuang
 * @Description:回到顶部
 * @Parameter:
 * @Date: 下午3:28 18/3/1
 */
function scrollD(i) {
    local.gotoPage(i);
    scroll_top('#mainSearchResultMiddle');
}
function scroll_top(dom){
    $(dom).animate({scrollTop: 0},'fast');

}
/**
 * @Author:WangShuang
 * @Description:辅助定位点击li 变为地图中心
 * @Parameter:
 * @Date: 下午4:13 18/3/1
 */
function changeCenter(lng,lat,title,address){

    map.setCenter(new BMap.Point(lng, lat));
    map.panTo(new BMap.Point(lng, lat));
    var markerSearch = new BMap.Marker(new BMap.Point(lng, lat));

    var sContent ='<p class="adressTitle">'+title+'</p>' +
        '<p class="adressDetail">'+address+'</p>';


    var opts = {
        enableMessage: false,
        offset:new BMap.Size(0,-20)
    };
    var infoWindow = new BMap.InfoWindow(sContent, opts);
    map.openInfoWindow(infoWindow,new BMap.Point(lng, lat));

}
$(".searchWord").click(function(e){
    // word = $("#searchInput").val();
    // local = new BMap.LocalSearch(map, {
    //     renderOptions:{map: map, panel:"mainSearchResult"},
    //     pageCapacity:10
    // });
    // local.searchInBounds(word, map.getBounds());
    // $('#mainSearchResult').css({"height":"500px","overflow-y":"scroll"});
    // $('#mainSearchResult').show();
    $('.page').empty();
    $('.page').remove();
    document.getElementById("mainSearchAdressItems").innerHTML = '<p class="adressDetail" style="margin-top: 10px;">数据加载中...</p>';
    word = $("#searchInput").val();

    var options = {
        onSearchComplete: function(results){
            console.log('results',results.tr);
            if(results.tr.length>0){
                removeSearchMarkers(searchAddMarkers);
                //清除标记
                // map.clearOverlays();
                //返回总页数
                var pageNum = results.getNumPages();
                //返回页数序号 ,从0开始
                var cPage = results.getPageIndex();
                //返回当前页的结果数
                var cPNum = results.getCurrentNumPois();

                //显示分页的页数
                var stepPage = 3;
                //开始页数
                var beginPage = 0;
                //结束页数
                var endPage = stepPage;

                var pageStr = '';
                var page = '';

                var firstPage = '<span><a href = "javascript:void(0)" onclick="local.gotoPage(0)">首页</a></span>';
                //上一页
                var preNum = 0;
                var prePage = '';
                //下一页
                var nextNum = 0;
                var nextPage = '';
                //var lastPage = '<span><a href = "javascript:void(0)" onclick="local.gotoPage('+(pageNum-1)+')">尾页</a></span>';
                // 判断状态是否正确
                if (local.getStatus() == BMAP_STATUS_SUCCESS){

                    // console.log(results);
                    var s = [];
                    var str = '';
                    // console.log(results.getNumPages());
                    // console.log(results.getNumPois());



                    var arr = [];
                    for (var i = 0; i < results.getCurrentNumPois(); i++){
                        // console.log(222,results.getPoi(i));
                        arr.push({lng:results.getPoi(i).point.lng, lat:results.getPoi(i).point.lat,title:results.getPoi(i).title,address:results.getPoi(i).address});
                        s.push(results.getPoi(i).title + ", " + results.getPoi(i).address);
                        var lng = results.getPoi(i).point.lng;
                        var lat = results.getPoi(i).point.lat;
                        var title = results.getPoi(i).title;
                        // var title = "1";
                        var address = results.getPoi(i).address;
                        // var address = "2";
                        str+='<li class="changeBg" data-lng="'+results.getPoi(i).point.lng+'" data-lat="'+results.getPoi(i).point.lat+'" onclick=\'changeCenter('+lng+','+lat+',\"'+title+'\",\"'+address+'\");\'>' +
                          '   <span>'+(i+1)+'</span>' +
                          '   <p class="adressTitle">'+results.getPoi(i).title+'</p>' +
                          '   <p class="adressDetail">'+results.getPoi(i).address+'</p>' +
                          '</li>';

                    }
                    searchAddMaker(arr);
                    //如果当前页小于显示页数，则开始页从第一页开始
                    if((cPage+1) < stepPage){
                        beginPage = 0;
                        endPage = stepPage + beginPage;
                    }
                    //如果当前页大于等于总页面数，则循环的时候结束页要为总页面数
                    else if((cPage+1) >= pageNum){
                        beginPage = cPage - 1;
                        endPage = pageNum;
                    }
                    //正常的时候，正常循环
                    else{
                        beginPage = cPage - 1;
                        endPage = stepPage + beginPage;
                    }

                    //如果当前页为0的时候，点击上一页就返回第0页
                    if(cPage - 1 < 0){
                        preNum = 0;
                    }else{
                        preNum = cPage - 1;
                    }
                    //如果当前页等于总页面数的时候，点击下一页返回最后一页
                    if(cPage+1 == pageNum){
                        nextNum = pageNum - 1;
                    }else{
                        nextNum = cPage + 1;
                    }


                    if (pageNum > 1) {
                        for ( var i = beginPage; i < endPage; i++) {
                            var j = i + 1;
                            if (i != cPage) {
                                pageStr += '<a class="pageStr"  href = "javascript:void(0)" onclick="scrollD('+i+')">' + j + '</a>';
                            }else{
                                // pageStr = pageStr + '[' + j + ']';
                                pageStr = pageStr + '<a class="pageStr pageStrCur"  href = "javascript:void(0)">' + j + '</a>';
                            }
                        }
                        //pageStr += ']';
                    } else if (pageNum == 1) {// 03/28/新增判断 S7
                        pageStr = '<a class="pageStr pageStrCur"  href = "javascript:void(0)">' + 1 + '</a>';
                    }

                    prePage = '<span><a class="pageStr" href = "javascript:void(0)" onclick="local.gotoPage('+preNum+')"><</a></span>';
                    nextPage = '<span><a class="pageStr" href = "javascript:void(0)" onclick="local.gotoPage('+nextNum+')">></a></span>';
                    //如果当前页小于显示页数，则开始页从第一页开始
                    if((cPage+1) < stepPage)
                    {
                        if(cPage == 0)
                        {
                            page = '<div class="page">' + pageStr + nextPage + '</div>';
                        }else{
                            page = '<div class="page">' + prePage + pageStr + nextPage + '</div>';
                        }
                    }else if((cPage+1) >= pageNum)
                    {
                        page = '<div class="page">' + firstPage + prePage + pageStr + nextPage + '</div>';
                    }else
                    {
                        page = '<div class="page">' + firstPage + prePage + pageStr + nextPage + '</div>';
                    }

                    // s.push(page);
                    document.getElementById("mainSearchAdressItems").innerHTML = str;
                    $('.page').empty();
                    $('.page').remove();
                    $("#mainSearchResult").append(page);

                }
            }else{
                document.getElementById("mainSearchAdressItems").innerHTML = '<p class="adressDetail" style="padding: 60px;">无结果</p>';
            }

        }
    };
    var local = new BMap.LocalSearch(map, options);
    window.local = local;
    local.search(word);

    $('#mainSearchResultMiddle').css({"width":"286px","min-height":"50px","max-height":"400px","overflow-y":"scroll"});
    $('#mainSearchResult').show();

});
$('#mainSearchAdressItems').on('mouseenter','li',function(){
    $(this).css('background','#efefef');
}).on('mouseleave','li',function(){
    $(this).css('background','#fff');
});
/**
 * @Author: WangShuang
 * @Description: 渲染筛选小区后的数据
 * @Parameter: result 需要渲染的数据
 * @Date:13:37 2018/2/3
 */
function villageHistoryRender(result){
    var code = result.code;
    var resultData={};
    if(code == 200){
        resultData = result.data;
        villageHistoryRenderHtml(resultData);
    }else{
        villageHistoryRenderHtml({});
        console.log('暂无数据');
    }
}

function villageHistoryRenderHtml(resultData){

    var str='';
    if(resultData.length>0){
        for(var i=resultData.length-1;i>=0;i--){
            var num = i+1;
            str+='<li data-area='+resultData[i].area+' data-polygon_points = '+resultData[i].polygon_points+' data-circle_marker='+resultData[i].circle_marker+' data-circle_center='+resultData[i].circle_center+' data-circle_radius='+resultData[i].circle_radius+' data-city_code='+resultData[i].city_code+' data-data_source='+resultData[i].data_source+' data-build_year='+resultData[i].build_year+' data-rooms='+ resultData[i].rooms+' data-id='+resultData[i].id+' data-name='+ resultData[i].name+' data-rent_money='+resultData[i].rent_money+' data-rent_way='+ resultData[i].rent_way+' data-user='+resultData[i].user+' data-village_id='+resultData[i].village_id+' data-village_id_effect='+resultData[i].village_id_effect+'>' +
                '     <span>'+ num +'   '+ resultData[i].name +'</span><img src="../images/dustbin.png" onclick="isSure('+'\''+resultData[i].id+'\''+')">' +
                '</li>';
        }
        $('#myCondition-information-items').html(str);
        //我的筛选条件
        $('.myCondition-information-items>li').hover(function(){
            var $this = $(this);
            $(this).addClass('colorBlue');
            $(this).children('img').show();
        },function(){
            $(this).removeClass('colorBlue');
            $(this).children('img').hide();
        });
        $('.myCondition-information-items').find('li').click(function(){
            dialogShow();
            judgeRight();
            closeMask();
            recoverDefault();
            var id = isUndefined($(this),'data-village_id');
            final_village_ids = id;
            var city_code = isUndefined($(this),'data-city_code');
            final_city_code = city_code;
            var data_source = isUndefined($(this),'data-data_source');
            final_data_source = data_source;
            var build_year = isUndefined($(this),'data-build_year');
            final_build_year = build_year;
            var rent_way = isUndefined($(this),'data-rent_way');
            final_rent_way = rent_way;
            var rooms = isUndefined($(this),'data-rooms');
            final_rooms = rooms;
            var rent_money = isUndefined($(this),'data-rent_money');
            final_rent_money = rent_money;
            var area = isUndefined($(this),'data-area');
            final_area = area;
            var circle_center = isUndefined($(this),'data-circle_center');
            var circle_marker = isUndefined($(this),'data-circle_marker');
            var circle_radius = isUndefined($(this),'data-circle_radius');
            var polygon_pointed = isUndefined($(this),'data-polygon_points');

            //给成员变量赋值
            if(circle_center != undefined){
                var circle_center_ed = circle_center.split(",");
                circle_point_lng = circle_center_ed[0],circle_point_lat=circle_center_ed[1];
                var circle_marker_ed = circle_marker.split(",");
                marker_point_lng = circle_marker_ed[0],marker_point_lat=circle_marker_ed[1];
            }

            var conditionArr = [data_source,build_year,rent_way,rooms,rent_money,area];
            for(var j=0;j< $('.mainDetailItems>li').length;j++){
                if(conditionArr[j] == 1){
                    conditionArr[j] = '普通住宅';
                }else if(conditionArr[j] == 2){
                    conditionArr[j] = '分散式公寓';
                }else if(conditionArr[j] == '1,2'){
                    conditionArr[j] = '普通住宅,分散式公寓';
                }else if(conditionArr[j] == '2,1'){
                    conditionArr[j] == '分散式公寓,普通住宅'
                }
                $('.mainDetailItems>li').eq(j).find('i').attr('data-type',conditionArr[j]);

                $('.mainDetailItems>li').eq(j).find('i').text(conditionArr[j]);
                var $dataType = $('.mainDetailItems>li').eq(j).find('i').attr('data-type');
                if($dataType){
                    var arr = $dataType.split(',');
                    for(var y=0;y<arr.length;y++){
                        $('.mainDetailItems>li').eq(j).find('li[data-type='+arr[y]+']').find('img').attr('src','../images/checkhave.png');
                    }

                }
            }
            getVillage(id,city_code,undefined,undefined,undefined,undefined,undefined,undefined,function (result) {
                console.log(result);
                if (result.code == 200 || result.code == 201){
                    lastTimeVillageData = result.data;
                    findByScreeningConditions(id ,city_code,data_source,build_year,rent_way,rooms,rent_money,area,circle_center,circle_radius,circle_marker,polygon_pointed);
                }else {
                    console.log("筛选条件创建图形出错");
                }
            });
        });
    }else if(!resultData.length){
        str='<li>' +
            '     <span style="text-align:center;color: #ccc;">暂无数据</span>' +
            '</li>';

        $('#myCondition-information-items').html(str);
    }
}
/**
 * @Author: WangShuang
 * @Description:  '房屋类型','租赁方式','居室','租金','面积' 恢复默认状态
 * @Date:16:18 2018/2/2
 */
function  recoverDefault(){
    var staticArr = ['房屋类型','年代','租赁方式','居室','租金','面积'];
    for(var j=0;j<staticArr.length;j++){
        $('.mainDetailItems>li').eq(j).find('i').text(staticArr[j]);
        $('.mainDetailItems>li').eq(j).find('i').attr('data-type','');
        $('.mainDetailItems>li').eq(j).children('.rentMethod').children('ul').children('li').find('img').attr('src','../images/checkempty.png');
    }
}
/**
 * @Author:W.s
 * @Description: 加载中
 * @Parameter:
 * @Date:10:06 2018/2/23
 */
function loading(){
    //$('#houseItems').html('');
    var $dom = '<li style="border: none;">' +
        '<img src="../images/loading.gif" style="width: 50%;position: relative;left: 25%;"/>' +
        '</li>';
    $('#houseItems').html($dom);
}
/**
 * @Author:W.s
 * @Description: data 数据源; wsIndex 小区索引; handle 排序回调;keyWs  租金,面积,居室;typeWs 1-升序  2-降序;
 * @Parameter:
 * @Date:10:06 2018/2/23
 */

function rightHouse(data, wsIndex, handle, keyWs, typeWs){
    console.log(data);
    $('#stretch1').children('img').attr('src', '../images/you.png');
    $('#paging').html('');
    var page = ' <div class="prev disable"><</div>' +
        '<ul class="list"></ul>' +
        '<div class="next">></div>' +
        '<div class="go">' +
        '<input type="text" placeholder="Goto">' +
        '<button>跳转</button>' +
        '</div>';
    $('#paging').html(page);
    var dataObj =[];
    if(Object.prototype.toString.call(data) === '[object Object]'){
        if(data.code == 200){
            dataObj = data.data;
        }else{
            console.log("暂无数据");
            dataObj = [];
        }

    }else{
        dataObj = data;
    }
    var arrForCount = [];
    var arrBuildYear = [];
    var arrObj = {};
    if(dataObj.length>0){
        $('#paging').show();
        for(var x=0;x<dataObj.length;x++){
            var year = dataObj[x].build_year;
            // arrBuildYear.push(dataObj[x].houses.length);

            for(var y=0;y<dataObj[x].houses.length;y++){
                arrBuildYear.push(year);
                arrForCount.push(dataObj[x].houses[y]);
                arrObj[x] = dataObj[x].houses[y];
            }
        }
        if(handle){
            handle(arrForCount,keyWs, typeWs);
        }
        console.log(arrForCount);
        $('#houseNum').text(arrForCount.length);
        var setTotalCount = arrForCount.length;
        var page = 20;
        var pageNum = 1 + '/' + Math.ceil(setTotalCount / page);
        $('#paging').paging({
            nowPage: 1,
            allPages: Math.ceil(setTotalCount / page),
            displayPage: 3,
            callBack: function (now) {
                var currentPages = now * page < setTotalCount ? page: setTotalCount - (now - 1) * page;
                var str ='';
                for (var i = 0; i < currentPages; i++) {
                    var num = (now - 1) * page + i;
                    var create_i = '';
                    if(arrForCount[i].data_source == 1){
                        create_i ='<i class="iconP"></i>';
                    }else if(arrForCount[i].data_source == 2){
                        create_i ='<i class="iconS"></i>';
                    }else{
                        create_i ='<i class="iconJ"></i>';
                    }
                    //判断字段
                    var rent_way = '';
                    if(arrForCount[num].rent_way == 0){
                        rent_way = '合租';
                    }else if(arrForCount[num].rent_way == 1){
                        rent_way = '整租';
                    }else if(arrForCount[num].rent_way == 2){
                        rent_way = '合租';
                    }
                    //Determine the number of years returned.
                    var year_con = '';
                    if ( arrBuildYear[num] == '0' ) {
                        year_con = '未知';
                    } else {
                        year_con = arrBuildYear[num];
                    }
                    //㎡
                    str += '<li class="clearfix wsHouseItem css3Li" data-index="'+wsIndex+'">' +
                        '      <span class="spanOne relative" title="'+arrForCount[num].village_name+'"> '+ create_i + arrForCount[num].village_name +'</span>' +
                        '      <span>'+ year_con +'</span>' +
                        '      <span>'+ rent_way +'</span>' +
                        '      <span>'+ arrForCount[num].rooms +'</span>' +
                        '      <span>'+ arrForCount[num].rent_money +'</span>' +
                        '      <span>'+ arrForCount[num].area +'</span>' +
                        '</li>';

                }
                // $('#houseItems').html('');

                $('#houseItems').html(str);
                for (let i = 0; i < $('#houseItems').find('li').length; i++ ) {
                    setTimeout(function() {
                        $('#houseItems').find('li').eq(i).animate({'opacity':'1', 'margin-left': '20px'},{ 'easing': 'swing' });
                    }, 100*i);
                }
                if(wsIndex == undefined){
                    $('.wsHouseItem').css('cursor','pointer');
                    $('.wsHouseItem').click(function(){
                        $('#back').show();
                        var arr1 = Object.keys(arrObj);
                        var len = arr1.length;
                        for(let s=0;s<len;s++){
                            if($(this).children('.spanOne').attr('title')== arrObj[s].village_name){
                                objWs = {};
                                objWs.data = [];
                                objWs.data[0] = dataObj[s];
                                objWs.code = 200;
                                final_village_ids = dataObj[s].id;
                                lastData = objWs;
                                wsIndex = s;
                                rightHouse(objWs,s);
                            }
                        }

                    });

                }
            }
        });
        $('#paging .list').on('click','li',function(){
            scroll_top('.mainMenuRightItems');//点击回到顶部
        });
    }else{
        //$('#NotypeBtn img').unbind();
        $('#houseNum').text('0');
        $('#houseItems').html('<div style="text-align: center;color: #ccc;border: none;margin-top: 10px;">暂无数据</div>');
        $('#paging').hide();
        $('#paging').html('');
    }
    showDiv();
}
if(data == '' ){
    getVillageFirst("110100",function (result) {
        data = result;
        $("#showOrHide").show();
        $("#myConditionResult").show();
        $("#mainMenuRight").show();
        hideRightOrUp('#mainMenuRight','right','-430px');
        showDiv();
    });
}
new WOW().init();
$('#paging').hide();

/**
 * @Author: yangfan
 * @Description: 点击按钮之后调用排序方法
 * @Parameter:
 * @Date: 下午15:16 18/4/2
 */
function DataSort(dom){
    $('#'+ dom).on('click','img',function(){
        if($.trim($(this).parent().parent().text()) == '居室'){
            if(typeBtn(this)){
                rightHouse(lastData,wsIndex,wsSort,'rooms',1);
            }else{
                rightHouse(lastData,wsIndex,wsSort,'rooms',2);
            }
        }else if($.trim($(this).parent().parent().text()) == '租金'){
            if(typeBtn(this)){
                rightHouse(lastData,wsIndex,wsSort,'rent_money',1);

            }else{
                rightHouse(lastData,wsIndex,wsSort,'rent_money',2);

            }
        }else{
            if(typeBtn(this)){
                rightHouse(lastData,wsIndex,wsSort,'area',1);

            }else{
                rightHouse(lastData,wsIndex,wsSort,'area',2);

            }
        }
        
    })
}
/**
 * @Author:R.c
 * @Description: 根据租金排序
 * @Parameter: sort_way 排序方式  houses_sort 排序数据
 * @Date:23:05 2018/4/3
 */
function wsSort(arrForCount, keyWs, typeWs){
    if (typeWs==1) {
        for (var w = 0; w < arrForCount.length; w++) {
            for (s = 0; s < arrForCount.length - 1 - w; s++) {
                if (arrForCount[s][keyWs] > arrForCount[s + 1][keyWs]) {
                    var temp = arrForCount[s];
                    arrForCount[s] = arrForCount[s + 1];
                    arrForCount[s + 1] = temp;
                }
            }
        }
    }else if(typeWs==2){
        for (var w = 0; w < arrForCount.length; w++) {
            for (s = 0; s < arrForCount.length - 1 - w; s++) {
                if (arrForCount[s][keyWs] < arrForCount[s + 1][keyWs]) {
                    var temp = arrForCount[s];
                    arrForCount[s] = arrForCount[s + 1];
                    arrForCount[s + 1] = temp;
                }
            }
        }
    }
    return arrForCount;
}

DataSort('habitable');          //居室点击事件
DataSort('dataRent');           //租金点击事件
DataSort('proportion');         //面积点击事件

/**
 * @Author: yangfan
 * @Description: 点击切换按钮升序，降序按钮
 * @Parameter:
 * @Date: 下午15:16 18/4/3
 */
function typeBtn(obj){
    var typeVlue = true;
    //点击当前的按钮给其父级添加颜色;
    $(obj).parent().parent().css('color','#387EF4').siblings().css('color','#666666');
    //如果点击的是当前的第一个img,第一个img切换蓝色按钮;
    if($(obj).hasClass('BtnUp')){             //此处为蓝色按钮
        typeVlue = true;
        $(obj).attr('src','../images/ChBlueUp.png').parent().parent().siblings().find('img.BtnUp').attr('src','../images/blackUp.png');
        $(obj).parent().siblings().find('img.BtnDown').attr('src','../images/NoBlueDown.png').parent().parent().siblings().find('img:last').attr('src','../images/blackDown.png');   //当前的第二个蓝色置灰的状态
    
    }else if($(obj).hasClass('BtnDown')){
        typeVlue = false;
        $(obj).attr('src','../images/blueDown.png').parent().parent().siblings().find('img.BtnUp').attr('src','../images/blackUp.png');   //当前的第一个蓝色置灰的状态
        $(obj).parent().siblings().find('img.BtnUp').attr('src','../images/BlueUp.png').parent().parent().siblings().find('img:last').attr('src','../images/blackDown.png');  
    
    }
    return typeVlue;
}

function NotypeBtn(){
    $('#NotypeBtn span').css('color','#666666');
    $('#NotypeBtn span div img.BtnUp').attr('src','../images/blackUp.png');
    $('#NotypeBtn span div img.BtnDown').attr('src','../images/blackDown.png');
}

/**
* @Author:R.c
* @Description: 排序控制层（采用责任链模式，参数一级一级往下传） 致杨帆姐姐 多学习
* @Parameter: sort_type：排序类型 1-租金 2-面积 3-居室 sort_way:1-升序 2-降序
* @Date:22:32 2018/4/3
*/
function sortController(sort_type,sort_way) {

    //控制层只负责调方法，永远不会出现逻辑代码。哪怕这个控制层只有一个方法，形式也要保留
    //责任链模式：splitData -调用-> sortByTypeAndWay -调用-> assembleData -调用->  rightHouse  各个方法只负责干一件事情
    //             拆分数据             开始排序               组装数据            渲染数据
    splitData(sort_type,sort_way);

}
/**
* @Author:R.c
* @Description: 拆分数据  将小区 与 房子分离开  并且小区作为字典方便组装数据时候查询
* @Parameter: sort_type：排序类型 1-租金 2-面积 3-居室 sort_way:1-升序 2-降序
* @Date:22:48 2018/4/3
*/
function splitData(sort_type,sort_way) {
    console.log(lastTimeRenderVillageDate);
    var sort_data = lastTimeRenderVillageDate;
    var village_sort = [];//小区集合
    var houses_sort = [];//房子集合
    for (var i = 0;i<sort_data.length;i++){
        for (var j = 0; j<sort_data[i].houses.length;j++){
            houses_sort.push(sort_data[i].houses[j]);
        }
        sort_data[i].houses = undefined;
        village_sort.push(sort_data[i]) ;
    }
    console.log("village_sort");
    console.log(village_sort);
    console.log("houses_sort");
    console.log(houses_sort);
    sortByTypeAndWay(houses_sort,village_sort,sort_type,sort_way);
}
/**
* @Author:R.c
* @Description: 开始排序
* @Parameter: village_sort：小区字典 ，houses_sort 需要排序的房子，sort_type 排序类型，sort_way 排序方式
* @Date:22:38 2018/4/3
*/
function sortByTypeAndWay(houses_sort,village_sort,sort_type,sort_way) {
    //判断根据sort_type选择排序类型
    if (sort_type == 1){
        houses_sort = sortByRentMoney(houses_sort,sort_way);
    }else if(sort_type == 2) {
        houses_sort = sortByArea(houses_sort,sort_way);
    }else if(sort_type == 3 ){
        houses_sort = sortByRooms(houses_sort,sort_way);
    }
    console.log("houses_sort:");
    console.log(houses_sort);
    assembleData(houses_sort,village_sort);
}
/**
* @Author:R.c
* @Description: 组装数据 因为王爽的righHouse 只接收固定格式的数据
* @Parameter: houses_sort 排序后的房子  village_sort 小区数据字典
* @Date:21:54 2018/4/3
*/
function assembleData(houses_sort,village_sort) {
    var dealWithedData = [];
    for (var i = 0; i < houses_sort.length; i++ ){
        var village_id = houses_sort[i].village_id;
        for (var j = 0;j < village_sort.length;j++){
            if (village_sort[j].id == village_id){         //遍历房子，根据房子的villag_id 在village_sort字典中找到小区数据
                console.log(houses_sort[i]);
                console.log(village_sort[j].houses);
                var village_sort_let = village_sort[j];
                village_sort_let.houses = [houses_sort[i]];
                dealWithedData.push(village_sort_let);       //得到从接口获取数据一样的格式的数据                break;
            }
        }
    }
    console.log("dealWithedData");
    console.log(dealWithedData);
    var json = { code:"200",msg:"successful",data:dealWithedData };
    console.log(json);
    rightHouse(json);
}


$('#stretch1').click(function(){
    animate('#mainMenuRight','#stretch1','right-430','right','0','-430px','zuo','you');
    showRighOrDown('#showOrHide','top','0');
});

function animate(showId,clickId,flag,dir,dirShow,dirHide,zuo,you){
    var nowObj = {};
    nowObj[dir] = dirShow;
    var nowObj1 = {};
    nowObj1[dir] = dirHide;
    if($(showId).css(dir) == dirHide){
        $(showId).animate(nowObj,"fast");
        $(clickId).children('img').attr('src', '../images/'+you+'.png');
    }else{
        $(showId).animate(nowObj1,"fast");
        $(clickId).children('img').attr('src', '../images/'+zuo+'.png');
    }
}
function showRighOrDown(id,dir,dirShow){
    var nowObj = {};
    nowObj[dir] = dirShow;
    $(id).animate(nowObj,"fast");
}
function hideRightOrUp(id,dir,dirHide){
    var nowObj = {};
    nowObj[dir] = dirHide;
    $('#stretch1').children('img').attr('src', '../images/zuo.png');
    $(id).animate(nowObj,"fast");
}
//菜单 点击每一项 切换图片 区分被选状态
var arr = [];
$('.mainDetailItems>li').each(function(index, item){
    $(this).hover(function(){
        $('.txt[name = min_area]').val('');
        $('.txt[name = max_area]').val('');
        $('.txt[name = min_price]').val('');
        $('.txt[name = max_price]').val('');
        var $this = $(this);
        $(this).children('.rentMethod').show();
        var $this1= $(this).children('.rentMethod').children('ul').children('li');
        var str = '';
        //存储上次点击的值
        if($this.children('span').children('i').attr('data-type')){
            arr = $this.children('span').children('i').attr('data-type').split(',');
        }else{
            arr = [];
        }
        $this1.click(function(){
            if($this.children('span').children('i').attr('data-type1')){
                $this.children('span').children('i').removeAttr('data-type1');
            }
            $(this).children('img').attr('src',$(this).children('img').attr('src')=='../images/checkempty.png'?'../images/checkhave.png':'../images/checkempty.png');
            $this.children('span').children('i').text('');
            if($(this).children('img').attr('src')=='../images/checkhave.png'){
                arr.push($(this).attr('data-type'));
            }else if($(this).children('img').attr('src')=='../images/checkempty.png'){
                arr.splice(arr.indexOf($.trim($(this).text())),1);
            }else {
                arr = ['不限'];
                $this.children('.rentMethod').hide();
            }
            if(arr && arr.length>0){
                $this.children('span').children('i').text(arr.join(','));
                $this.children('span').children('i').attr('data-type',arr);

            }else{
                $this.children('span').children('i').text($this.children('span').children('i').attr('data-name'));
                $this.children('span').children('i').removeAttr('data-type');

            }

        });
    },function(){
        $('#area,#price').hide();
        var $this1= $(this).children('.rentMethod').children('ul').children('li');
        $(item).children('.rentMethod').hide();
        $this1.unbind('click');
    });

});
//输入面积或价格后点击确定按钮
function priceAreaBtn (id, min,max,name){

    var small = $('.txt[name = '+min+']').val();
    var big = $('.txt[name = '+max+']').val();
    if($.trim(small)  != ''&&$.trim(big) != '' && Number(small)>=0&&Number(big)>=0&&Number(big)>Number(small)){
        $('#'+id).show();
        $('#'+id).click(function(){
            var itemName = $('i[data-name='+name+']');
            itemName.parents('li').children('.rentMethod').children('ul').children('li').find('img').attr('src','../images/checkempty.png');
            if(id == 'area'){
                itemName.text(small+'-'+big+'㎡');
                itemName.parents('li').children('span').children('i').removeAttr('data-type').attr('data-type1',small+'-'+big+'㎡');
            }else if(id == 'price'){
                itemName.text(small+'-'+big);
                itemName.parents('li').children('span').children('i').removeAttr('data-type').attr('data-type1',small+'-'+big);
            }
            $('#'+id).hide();

        });

    }else{
        $('#'+id).hide();
    }
};
$('#area,#price').hide();
$('.txt[name = min_area],.txt[name = max_area]').on('input',function () {
    priceAreaBtn ('area', 'min_area','max_area','面积');
});
$('.txt[name = min_price],.txt[name = max_price]').on('input',function () {
    priceAreaBtn ('price', 'min_price','max_price','租金');
});
//点击清空F
$('#del').click(function(){
    // test('清空');
    globRadius = 1000;
    clearInput(); //隐藏输入框
    removeRightHouse();
    removeEchart();//清除Echarts
    recoverDefault();
    removeAll();
    removeRichMarker(myRichMarkersData);
    myRichMarkersData = [];
    reSetDownloadData();
    hideDiv();
    NotypeBtn();
});

/**
 * @Author:WangShuang
 * @Description: 按钮点击效果
 * @Parameter:
 * @Date: 下午5:54 18/2/6
 */
function clickEffect(dom){
    if(!$(dom).is(':animated')){
        var flag = true;
        $(dom).animate({
            opacity:  0.8
        }, 100,function() {
            $(dom).css({opacity: 1});
        });
    }
}

function sourceData(){
    var parameters = [];
    parameters.push(getCity());
    for(var j=0;j< $('.mainDetailItems>li').length;j++){
        var $dataType = $('.mainDetailItems>li').eq(j).find('i').attr('data-type');
        if($dataType == '普通住宅'){
            $dataType = 1;
        }else if($dataType == '分散式公寓'){
            $dataType = 2;
        }else if($dataType == '普通住宅,分散式公寓'||$dataType == '分散式公寓,普通住宅'){
            $dataType = '1,2';
        }
        if($('.mainDetailItems>li').eq(j).find('i').attr('data-type1')){
            $dataType = $('.mainDetailItems>li').eq(j).find('i').attr('data-type1')
        }
        parameters.push($dataType==''?undefined:$dataType);
    }
    return parameters;
}

//点击确定
$('#sure').click(function(){
    $('#back').hide();
    NotypeBtn();
    if(isExist()){
        dialogShow();
        judgeRight();
        screenVillage(sourceData());
    }else{
        test('请在地图上设定筛选范围！');
    }
    clickEffect('#sure');
});
//鼠标划过搜索
$('#mainSearch').hover(function(){
    $('.mainSearchInput').css('display', 'inline-block');
    // $('.mainSearchResult').show();
    //搜索时无结果则不展示结果列表
    if($.trim($('#searchInput').val()) != ''){
        $('.mainSearchResult').show();
    }else{
        $('.mainSearchResult').hide();
    }
},function(){
    clearTimeout(timer1);
    if($.trim($('#searchInput').val()) != '') {
        $('.mainSearchInput').css('display', 'inline-block');
    }else {
        var timer1 = setTimeout(function() {
            $('.mainSearchInput').css('display', 'none');
            $('.mainSearchResult').hide();
        }, 800);
    }

});
//输入框输入内容后出现X
$('.clearInput').hide();

$('#searchInput').on('input propertychange', function(){
    if($.trim($('#searchInput').val()) != ''){
        $('.clearInput').show();

    }else{
        $('.clearInput').hide();

        //清除检索数据
        if(local){
            local.clearResults();
            $('.mainSearchResult').hide();
        }
    }
});

//添加回车监听事件
$('#searchInput').bind("keydown",function (e) {
    var key = e.which;  //获得按键的ascii码
    if (key == 13) {
        //回车后进行搜索
        $(".searchWord").click();
    }

});


$('.clearInput').click(function(){
    $('#searchInput').val('');
    $('.clearInput').hide();
    $(".mainSearchResult").hide();
    if(local){
        local.clearResults();
    }
    if(searchAddMarkers.length>0){
        removeSearchMarkers(searchAddMarkers);
    }
});




//点击保存筛选条件
$('#mainSave').click(function(){
    if(isExist()){
        $('#saveCondition').show();
    }else{
        test('请在地图上设定筛选范围！');
    }
    clickEffect('#mainSave');
});
//点击取消
$('.cancel').click(function(){
    removeRed();
    test('已取消');
    /*setTimeout(function(){*/
    $('#saveCondition').hide();
    /*},100);*/
    $('#saveInput').val("");
});
//点击保存
$('.save').click(function(){
    var name = $('#saveInput').val();
    if(name != ''&& isExist()){
        saveVillageHistory(sourceData(),name);
        /*setTimeout(function(){*/
        $('#saveCondition').hide();
        /*},100);*/
        $('#saveInput').val("");
    }else{
        addRed();
    }

});

/**
 * @Author:WangShuang
 * @Description:是否为undefined
 * @Parameter:
 * @Date: 上午10:09 18/2/2
 */
function isUndefined($dom, data_attr){
    return  $dom.attr(data_attr) == 'undefined'||$dom.attr(data_attr) == 'null' ?undefined:$dom.attr(data_attr);
}
/**
 * @Author: wang shuang
 * @Description:
 * @Parameter:
 * @Date:10:52 2018/2/28
 */
function getCity() {
    return $("#city input").attr("data-code");
}
/**
 * @Author:WangShuang
 * @Description: 点击我的筛选条件的X 关闭遮罩层
 * @Parameter:
 * @Date: 上午10:10 18/2/2
 */
function closeMask(){
    $('#myCondition').hide();
    $('#myConditionResult').css('z-index',10);
}
//点击三个横条按钮
$('#myConditionResult').click(function(){
    $('#inputCondition').val('');
    findVillageHistory();
    $('#myCondition').show();
    $('#myConditionResult').css('z-index',100);
});


$('body').on('click','.X',function() {
    if($('.isdele').length == 0) {
        closeMask();
    }

});

//toast提示框
var toastTime2=null;
var displayTime2=null;
function setToast3(html) {
    if (toastTime2 != null) {
        clearTimeout(toastTime2);
        clearTimeout(displayTime2);
    }
    $('#toastId2').show();
    $('#toastId2').css('opacity',1);
    $('#toastId2').html(html);
    toastTime2=setTimeout(function(){
        $('#toastId2').css('opacity',0);
        displayTime2=setTimeout(function(){$('#toastId2').hide();},3000);
    },1000);
}


function test(str){
    setToast3('<div style="color:#fff;background: rgba(0, 0, 0, 0.6);border-radius: 2px;padding: 10px;text-align: center;margin: 0 auto;">'+str+'</div>');
}

function myBrowser(){
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
    var isOpera = userAgent.indexOf("Opera") > -1;
    if (isOpera) {
        return "Opera"
    }; //判断是否Opera浏览器
    if (userAgent.indexOf("Firefox") > -1) {
        return "FF";
    } //判断是否Firefox浏览器
    if (userAgent.indexOf("Chrome") > -1){
        return "Chrome";
    }
    if (userAgent.indexOf("Safari") > -1) {
        return "Safari";
    } //判断是否Safari浏览器
    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
        return "IE";
    }; //判断是否IE浏览器
}

//以下是调用上面的函数
var mb = myBrowser();
if ("IE" == mb) {
    console.log("我是 IE");
}
if ("FF" == mb) {
    console.log("我是 Firefox");
}
if ("Chrome" == mb) {
    console.log("我是 Chrome");
}
if ("Opera" == mb) {
    console.log("我是 Opera");
}
if ("Safari" == mb) {
    console.log("我是 Safari");
    // $('html').css('zoom', 0.8);
}
/**
 * @Author:WangShuang
 * @Description:鼠标经过显示按钮文字描述
 * @Parameter:
 * @Date: 上午11:30 18/3/19
 */
function iconTitle(dom){
    $('#'+ dom).on('mouseenter',function(){
        $(this).children('.iconTitle').show();
    }).on('mouseleave',function(){
        $(this).children('.iconTitle').hide();
    });
}
//点击右侧列表返回按钮,重新渲染全部数据
function backAll() {
    $('#back').click(function(){
        backWs();
        NotypeBtn();
    })
}
//点击返回按钮,执行函数
function backWs(){
    $('span').removeClass('redBorder');
    $('.BMapLabel').removeClass('redActive');
    $('#back').hide();
    if (userCheeseStatus == 0) {
        $('.dataChart').addClass('hideEchart');

    } else if (userCheeseStatus == 1) {
        $('.dataChart').removeClass('hideEchart');
        $('#mainMenuRight').animate({'right':'0'},"fast");
    }
    rightHouse(lastTimeRenderVillageDate);
    getEchartData(lastTimeRenderVillageDate);
    if(lastTimeVillageData.length>0){
        var id = '';
        for(var i = 0;i <= lastTimeVillageData.length - 1;i++){
            id += lastTimeVillageData[i].id + ","
        }
        id = id.substr(0,id.length-1);//去掉最后一个空格
        id = isList(id);//判断长度是否大于0，否则赋值为 undefined
        final_village_ids = id;
    }
}
/**********************************************  王爽end  ***************************************************8*/
/**********************************************  杨帆begin  ***************************************************8*/

/**
* @Author: Y.f
* @Description: 模态框演示
* @Parameter:
* @Date:17:39 2018/2/24
*/
function dialogShow() {
    $('.dialog').show();
}
/**
 * @Author: Y.f
 * @Description: 模态框隐藏
 * @Parameter:
 * @Date:17:39 2018/2/24
 */
function dialogHide() {
    $('.dialog').hide();
}

//定义需要用到的变量
var markersAll = {
    polygon: null                       //多边形对象
};


/**
* @Author:R.c
* @Description: 通过百度插件创建多边形
* @Parameter:
* @Date:15:42 2018/3/21
*/
function createPolygonByBaidu(polygon) {
    markersAll.polygon = polygon ;
}


/**
* @Author: R.c Y.f
* @Description: 通过搜索条件创建多边形
* @Parameter:
* @Date:11:11 2018/3/5
*/
function createPolygonBySearchVillage(polygon_points) {
    var arrPoints = dealWithPolygonPointsData(polygon_points);
    markersAll.polygon = new BMap.Polygon(arrPoints, {
        strokeColor:"blue",
        strokeWeight:2,
        strokeOpacity:0.65,
        fillColor:"blue",
        fillOpacity: 0.1
    });
    map.addOverlay(markersAll.polygon);
    exist = true;                              //标记绘制图形是否存在
    isCircleOrPolygon = 1;                      //标记当前图形是多边形
}
/**
* @Author:R.c
* @Description: 处理多边形数据
* @Parameter:
* @Date:11:19 2018/3/5
*/
function dealWithPolygonPointsData(polygon_points) {
    var arrPoints = [];                          //经纬度坐标点数组
    var points = polygon_points.split(";");
    var point = ""                              //经纬度坐标点
    for (var i = 0;i<points.length;i++){
        point = points[i].split(",");
        arrPoints.push(new BMap.Point(point[0], point[1]));
    }
    return arrPoints;
}
/**
 * @Author:R.c
 * @Description: 拿到所有要保存的多边形经纬度点 lat，lng；lat，lng；
 * @Parameter:
 * @Date:10:42 2018/3/5
 */
function getPolygonPoint() {
    var point_str = "";
    console.log(markersAll.polygon.arrPoint);
    var points = markersAll.polygon.getPath();
    if (points.length>0){
        for (var i =0 ; i<points.length;i++){
            console.log(points[i]);
            point_str += points[i].lng + "," + points[i].lat + ";";
        }
        point_str = point_str.substr(0,point_str.length-1);//去掉最后一个 ';'
        point_str = isList(point_str);
    }else {
        point_str = undefined;
    }
    return point_str;
}


//清除上一次的数据和多边形;
function removePolygon() {
    //判断多边形是否存在
    window.markersAll.polygon && map.removeOverlay(window.markersAll.polygon);
    //初始化数据
    markersAll.polygon = null;      //多边形对象
    exist = false;                  //标记圆或多边形不存在

}

//判断是否在多边形内；
function inPolygon() {
    var markersAllPoints = [];
    var markersAllMarkers = [];
    var isExistVillage = [];
    markersAll.lastVillageData = [];
    if (data != null && data.code == 200){
        var villages = data.data;
        var pointPolygon = '';
        for(var i =0;i<villages.length;i++) {
            pointPolygon = new BMap.Point( getnum(villages[i].longitude), getnum(villages[i].latitude) );
            if ( BMapLib.GeoUtils.isPointInPolygon(pointPolygon,markersAll.polygon) ) {   //判断是否在多边形内方法;
                markersAllPoints[i] = pointPolygon;
                markersAll.lastVillageData.push(villages[i]);
                isExistVillage.push(villages[i]);
            }
        }
        getVillage(getIds(isExistVillage),getCity(),undefined,undefined,undefined,undefined,undefined,undefined,function (result) {
            if (result.code == 200){
                lastTimeVillageData = result.data;
                lastTimeRenderVillageDate = result.data;//根据用户选择渲染
                userCheese();
                setDownloadData();
                myRichMarkersData = lastTimeVillageData;
                addRichMarker(myRichMarkersData);
                dialogHide();
            }else if(result.code == 201){
                removeEchart();//清除Echarts数据
                dialogHide();
                removeRightHouse();
            }else {
                console.log("inPolygon 出错");
            }
        });
        markersAllMarkers = addValidMarkers(markersAllPoints);
    }
    return markersAllMarkers;
}

/**
* @Author: 杨帆
* @Description:  点击编辑给输入框赋值
* @Parameter:
* @Date: 18:17 2018/3/23
*/
function InputValue() {
    var reg = new RegExp('</span>', 'g');
    label.content.substr(47).replace(reg, '');
    var oldInputVal =label.content.substr(47).replace(reg, '');
    oldInputVal = Number(oldInputVal.substr(0,oldInputVal.length-1));
    oldInputVal = parseInt(oldInputVal);
    $("#getValue").val(oldInputVal);
}
/**
 * @Author: 杨帆
 * @Description:  编辑圆上的距离
 * @Parameter:
 * @Date: 14:25 2018\3\13
 */

function EditorMarker(){
    /*label.removeEventListener("click", EditorMarker);
    var labelContent = '<div contenteditable="true">123</div>';
    label.setContent(labelContent);
    var p = e.target;
    var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);*/

    var pointToPixel = map.pointToPixel(label.getPosition());
    console.log(pointToPixel);

    InputValue();
    $('.EditorMarkerDiv').css({
        'display': 'inline-block',
        'left': (pointToPixel.x + 10) + 'px',
        'top': (pointToPixel.y - 14)+ 'px'
    });
    disableMap();       //百度地图禁止拖动，放大
    $('.Markerfinish').one("click", EditorMarkerVal);
    //point.html($('.mainSearchInput'));
}

/**
 * @Author: 杨帆
 * @Description:  点击完成赋值并重新渲染圆
 * @Parameter:
 * @Date: 13:46 2018\3\14 0014
 */
function EditorMarkerVal() {
    if($.trim($('.EditorMarkerInput').val()) == ''){
        return false;
    }else if($('.EditorMarkerInput').val().indexOf('.') !== -1){
        test('请输入整数');
        clearInput();
    }else{
        globRadius = parseFloat($('.EditorMarkerInput').val());
        if( globRadius>=1 && globRadius<=5000){
            editCircle();
            var labelText = '<span style="background: #efefef;padding: 4px">'+globRadius+'</span>';
            label.setStyle({ color : "black",borderColor : "white",fontSize : "12px" ,backgroundColor:"white",boxShadow: "1px 1px 5px #aaa",padding: "4px 6px",borderRadius: "3px"});     //设置拖动点标签样式
            label.setContent(labelText+' 米');
            $("#getValue").val(Number(globRadius));
        } else{
            test('请输入数字,范围为1-5000');
            clearInput();
        }

    }



    map.enableDragging();       //启用拖动
    map.enableScrollWheelZoom();     //启用放大
    NotypeBtn();
    //clearInput(); //隐藏输入框
}

/**
* @Author:WangShuang
* @Description: 输入框消失，且输入框内的值清空
* @Parameter:
* @Date: 上午10:45 18/3/15
*/
function clearInput(){
    $('.EditorMarkerDiv').css('display','none');
    $('.EditorMarkerInput').val('');
}


/**
 * @Author: 杨帆
 * @Description:  百度地图禁止拖动，放大
 * @Parameter:
 * @Date: 13:46 2018\3\14 0014
 */
function disableMap() {
    map.disableDragging();       //禁止拖动
    map.disableScrollWheelZoom();     //禁止放大
}
/**
* @Author:R.c
* @Description:  启动百度地图拖动，放大
* @Parameter:
* @Date:14:28 2018/3/22
*/
function enableMap() {
    map.enableDragging();       //启用拖动
    map.enableScrollWheelZoom();     //启用放大
}

/**********************************************  杨帆end  ***************************************************8*/
/********************************************** 王国馨begin ******************************************************/
function isSure(id) {
    console.log(id);
    event.stopPropagation();
    var _this = $(this);
    var strAlert = '<div class="isdele" style="display: block;background: rgba(0,0,0,.2);">'+
      '<div class="cheeseBox">'+
        '<p>您确定要删除吗？</p >'+
        '<div class="controlBox">'+
        '<span id="sureDelete">确定</span>'+
        '<span id="notDelete">取消</span>'+
        '</div>'+
        '</div>'+
        '</div>';
    $('#myCondition').append(strAlert);
    $('#sureDelete').on('click',function(){
        isSureDelete(id);
    });
    $('#notDelete').on('click',function(){
        notDelete();
    });

}
/**
 * @Author:王国馨
 * @Description:
 * @Parameter:
 * @Date: 18:58 2018/2/26
 */
function isSureDelete(id) {
    /*console.log("deleteID");
    console.log(id);*/
    $('.isdele').remove();
    deleteVillageSearchHistory(id);
}
/**
 * @Author:王国馨
 * @Description:
 * @Parameter:
 * @Date: 18:58 2018/2/26
 */
function notDelete() {
    $('.isdele').remove();
}

$('.mark-information .mark-message input').on('keyup',function(){
    if($(this).val() != ''){
        removeRed()
    } else {
        addRed()
    }
});
function addRed() {//添加红色
    $('.mark-information .mark-message input').addClass('red');
}
function removeRed() {//取消红色
    $('.mark-information .mark-message input').removeClass('red');
}
//判断'#mainMenuRight是否收起
function  judgeRight(){
    if( $('#mainMenuRight').right != 0){
        $('#mainMenuRight').animate({'right':'0'},"fast");
    }
}
//处理地址栏参数
function GetQueryString() {
    var url = window.location.search;
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        var searchArr = {};
        if (str.indexOf("&") != -1){
            var strs = str.split("&");
            var key = new Array(strs.length);
            var value = new Array(strs.length);
            for (var i = 0; i < strs.length; i++) {
                key[i] = strs[i].split("=")[0];
                value[i] = decodeURIComponent(strs[i].split("=")[1]);
                // alert(key[i]+"="+value[i]);
                searchArr[key[i]] = value[i];
            }
        }else {
            var arr = str.split("=");
            searchArr[arr[0]] = arr[1];
        }
    }
    return searchArr;
}
/**
* @Author:王国馨
* @Description:
* @Parameter:
* @Date: 10:35 2018/3/19
*/
/*点击切换柱状图*/
$('#echartItem').on('click',function(){
    $('.dataChart').removeClass('hideEchart');
    userCheeseStatus = 1;
    userCheese();
    NotypeBtn();
});
/*点击切信息列表*/
$('#houseInfoList').on('click',function(){
    userCheeseStatus = 0;
    $('.dataChart').addClass('hideEchart');
    userCheese();
    NotypeBtn();
});

/*根据用户选择状态控制渲染柱状图*/
function userCheese () {
   console.log(isUndefined($('.wsHouseItem'), 'data-index'));
    if (userCheeseStatus == 0) {
        $('.dataChart').addClass('hideEchart');
        if(isUndefined($('.wsHouseItem'), 'data-index') == undefined){
            rightHouse(lastTimeRenderVillageDate);
        }
    } else if (userCheeseStatus == 1) {
        $('.dataChart').removeClass('hideEchart');
        if(isUndefined($('.wsHouseItem'), 'data-index') == undefined){
            getEchartData(lastTimeRenderVillageDate);
        }else{
            getEchartData(objWs,$('.wsHouseItem').attr('data-index'));
        }
        $('#mainMenuRight').animate({'right':'0'},"fast");
    }
}
//Echarts不显示数据
function removeEchart() {
    lastTimeRenderVillageDate = [];
    getEchartData(lastTimeRenderVillageDate);
    // userCheese();
}
/*************************************************** 王国馨end ******************************************************/
//添加控件
add_control();
//切换城市
changeCity(citydata);
//鼠标经过右侧上部图标显示文字说明
iconTitle('downLoadAll');
iconTitle('houseInfoList');
iconTitle('echartItem');
//初始化地图以及监听事件
mapAddEnentListener();
//初始化地图绘制插件
setDrawingManager();
backAll();

