 var base = "http://192.168.37.18:9080/rentmap"; //base 网关地址
//var base = "http://10.5.120.175:9080/rentmap"; //base 网关地址
var getVillage_url =  base + "/rentmap/village"; //获取所有小区
var findVillageSearchHistory_url = base + "/rentmap/findVillageSearchHistory"; // 查询小区地址
var saveVillageSearchHistory_url = base + "/rentmap/insertVillageSearchHistory"; // 保存小区筛选数据
var getVillageFirst_url = base + "/rentmap/getVillageByCity"; // 第一次加载-获取所有小区
var deleteVillageSearchHistoryAjax_url = base + "/rentmap/deleteVillageSearchHistory"; // 删除小区历史记录
var findVillageSearchHistoryByLuenceAjax_url = base + "/rentmap/findVillageSearchHistoryByLike";  // 搜索查询
/**
 * @Author:R.c
 * @Description: 获取所有小区
 * @Date:14:34 2018/1/16
 */
function getVillage(id,city_code,data_source,build_year,rent_way,rooms,rent_money,area,handle) {
    var type = "post";
    var url = getVillage_url;
    var data = {
        village_ids: id,                  //小区ID
        city_code: city_code,              //城市编码
        data_source: data_source,         //房屋类型
        build_year: build_year,           //年代
        rent_way: rent_way,               //出租方式
        rooms: rooms,                     //户型
        rent_money: rent_money,           //租金
        area: area                        //面积
    };
    ajax(type,url,data,handle);
}
/**
 * @Author:R.c
 * @Description: 查询小区筛选数据
 * @Parameter: user:用户id
 * @Date:13:13 2018/2/1
 */
function findVillageSearchHistory( user ,city_code , handle) {
    var type = "post";
    var url = findVillageSearchHistory_url ;
    var data = {
        user : user,  //用户id
        city_code : city_code  //城市
    }
    ajax(type,url,data,handle);
}
/**
 * @Author:R.c
 * @Description: 保存小区筛选数据
 * @Parameter: user:所保存的筛选条件名 name:所保存的筛选条件名,village_ids:小区数组,city_code：城市编号, data_source：数据来源, rent_way：出租方式,
 *               rooms:户型,  rent_money：租金, area：面积, circle_center：圆心, circle_radius：圆半径
 * @Date:13:15 2018/2/1
 */
function saveVillageSearchHistory( user,name,village_ids , city_code , data_source , build_year, rent_way,
                                   rooms, rent_money, area, circle_center, circle_marker ,circle_radius,polygon_points,handle) {
    console.log(user);
    var type = "post";
    var url = saveVillageSearchHistory_url ;
    var data = {
        name : name,                    //所保存的筛选条件名
        user : user ,                   //所保存的用户id
        village_ids : village_ids,      //小区数组
        city_code : city_code,          //城市编号
        data_source : data_source,      //数据来源
        build_year: build_year, //年代
        rent_way : rent_way,            //出租方式
        rooms : rooms,        //户型
        rent_money : rent_money,        //租金
        area : area,                    //面积
        circle_center : circle_center,  //圆心
        circle_marker : circle_marker,  //圆上的标注
        circle_radius : circle_radius,  //圆半径
        polygon_points: polygon_points  //多边形的经纬度
    }
    ajax(type,url,data,handle);
}

/**
 * @Author:R.c
 * @Description: 获取所有小区 同步
 * @Date: 14:34 2018/1/16
 */
function getVillageFirst(city_code,handle) {
    var type = "post";
    var url = getVillageFirst_url;
    var data = {
        city_code:city_code              //城市编码
    };
    ajaxFirst(type,url,data,handle);
}

/**
 * @Author:W.s.k
 * @Description:删除小区历史数据
 * @Parameter:
 * @Date:14:55 2018/2/3
 */
function deleteVillageSearchHistoryAjax(id,handle) {
    var type = "post";
    var url = deleteVillageSearchHistoryAjax_url;
    var data = {
        id : id                    //所保存的筛选条件id
    }
    ajax(type,url,data,handle);
}
/**
 * @Author:R.c
 * @Description: 搜索查询
 * @Parameter:
 * @Date:0:55 2018/2/5
 */
function findVillageSearchHistoryByLuenceAjax(searchInput,user,city_code,handle) {
    var type = "post";
    var url = findVillageSearchHistoryByLuenceAjax_url;
    var data = {
        user: user ,                    //用户
        searchInput: searchInput,        //搜索内容
        city_code:city_code
    }
    ajax(type,url,data,handle);
}

/**
 * @Author:R.c
 * @Description: 封装了一个ajax方法
 * @Date:14:34 2018/1/16
 */
function ajax(type,url,data,handle) {
    console.log(data);
    var result = undefined;
    $.ajax({
        type:type,
        url:url,
        async:true,
        /*data:JSON.stringify(data),*/
        data:data,
        dataType:"json",
        beforeSend:function (request) {
            /*request.setRequestHeader("Content-Type", "application/json");*/
            // dialogShow();
        },
        success:function (data) {
            handle(data);
            result = data;
        },
        complete:function () {
            /* if (handle){
                 handle();
             }*/
        }
    });
}

/**
 * @Author:R.c
 * @Description: 封装了一个ajax方法 异步
 * @Date:14:34 2018/1/16
 */
function ajaxFirst(type,url,data,handle) {
    $.ajax({
        type:type,
        url:url,
        async:true,
        /*data: JSON.stringify(data),*/
        data: data,
        dataType:"json",
        beforeSend:function (request) {
            /*request.setRequestHeader("Content-Type", "application/json");*/
            dialogShow();
        },
        success:function (data) {
            handle(data);
        },
        complete:function () {
            dialogHide();
        }
    });
}

/**************Echarts部分*******************************/
function getAjax1 (obj) {
    /*第一个图表（不同居室租金(整租））ajax请求*/
    $.ajax({
        url: base + '/rentmap/getHouseTypeDistribution',
        type: 'POST',
        /*data: JSON.stringify(obj),*/
        data: obj,
        beforeSend:function (request) {
            /*request.setRequestHeader("Content-Type", "application/json");*/
            // dialogShow();
        },
        success: function (databack) {
            renderEchart1(databack);
        }
    });
}
function getAjax2 (obj) {
    /*第二个图表（不同面积段租金）ajax请求*/
    $.ajax({
        url: base + '/rentmap/getAreaParagraphRental',
        type: 'POST',
        /*data: JSON.stringify(obj),*/
        data: obj,
        beforeSend:function (request) {
            /*request.setRequestHeader("Content-Type", "application/json");*/
            // dialogShow();
        },
        success: function (databack) {
            // console.log(databack);
            renderEchart2(databack);
        }
    })
}
function getAjax3 (obj) {
    /*第三个图表（不同面积段租金）ajax请求*/
    $.ajax({
        url: base + '/rentmap/getHousingSupply',
        type: 'POST',
        /*data: JSON.stringify(obj),*/
        data: obj,
        beforeSend:function (request) {
            /*request.setRequestHeader("Content-Type", "application/json");*/
            // dialogShow();
        },
        success: function (databack) {
            // console.log(databack);
            renderEchart3(databack);
        }
    })
}
function getAjax4 (obj) {
    /*第四个图表（地铁房租金）ajax请求*/
    $.ajax({
        url: base + '/rentmap/getMetroHouse',
        type: 'POST',
        /*data: JSON.stringify(obj),*/
        data: obj,
        beforeSend:function (request) {
            /*request.setRequestHeader("Content-Type", "application/json");*/
            // dialogShow();
        },
        success: function (databack) {
            // console.log(databack);
            renderEchart4(databack);
        }
    });
}
