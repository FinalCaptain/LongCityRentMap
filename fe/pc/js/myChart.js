/**
 * Created by 17611 on 2018/3/13.
 */
/*获取页面数据信息*/
function getEchartData (dataEchart) {
    dataEchart = dataEchart.data || dataEchart;
    // console.log(dataEchart);
    var paramsid = '';
    var cityCode = '';
    for (var i = 0; i< dataEchart.length; i++) {
        var houses = dataEchart[i].houses;
        for (var j = 0; j< houses.length; j++) {
            paramsid += houses[j].id+",";
        }
        cityCode = dataEchart[i].city_code;
    }
    // console.log(cityCode + '==============' + paramsid);
    var obj = {"cityCode":cityCode,"paramsid":paramsid};
    roomsCount(dataEchart);//房源数量
    if ($('.rentType i').text() != '合租'){
        getAjax1(obj);
        getAjax2(obj);
        getAjax3(obj);
        getAjax4(obj);
        $('.different-bedroom').parent().show();
        $('.different-filter').parent().show();
    }else {
        getAjax2(obj);
        getAjax3(obj);
        $('.different-filter').parent().hide();
        $('.different-bedroom').parent().hide();
    }
}
//房源数量
function roomsCount(dataEchart) {
    var arrForCount = [];
    for(var x=0;x<dataEchart.length;x++){
        for(var y=0;y<dataEchart[x].houses.length;y++){
            arrForCount.push(dataEchart[x].houses[y]);
        }
    }
    $('#houseNum').text(arrForCount.length);
}

/*渲染第一个图表（不同居室租金(整租））*/
function renderEchart1(databack) {
    difBedrom(databack);
}
/*渲染第二个图表（不同面积段租金）*/
function renderEchart2(databack) {
    difArea(databack);
}
/*渲染第三个图表（不同居室供应量）*/
function renderEchart3(databack) {
    difAmount(databack);
}
/*渲染第四个图表（地铁房租金）*/
function renderEchart4(databack) {
    difFilter(databack);

}


/*封装一个截取字符串方法*/
// console.log(numFixed(12.34567890));
function numFixed (str) {
    var newStr = str + '';
    var finalStr = '';
    if (newStr.indexOf('.') != -1){
        finalStr = newStr.substr(0,newStr.indexOf('.')+3);
    } else {
        finalStr = newStr;
    }
    return finalStr;
}
//数组去重方法
function uniq(array){
    var temp = []; //一个新的临时数组
    for(var i = 0; i < array.length; i++){
        if(temp.indexOf(array[i]) == -1){
            temp.push(array[i]);
        }
    }
    return temp;
}

/*第一个图表（不同居室租金(整租））*/
function difBedrom(databack) {
    // console.log(databack);
    // console.log(databack.data);
    var rooms = ['开间','一居','两居','三居及以上'];  //x轴刻度
    var len = rooms.length;
    var roomMean = new Array(len); //整套租金
    var riceMean = new Array(len);  //每平米租金
    for (var i=0;i<len;i++) {
        riceMean[i] = 0;
        roomMean[i] = 0;
    }
    if (databack.data != undefined) {
        // console.log(databack.data);
        $.each(databack.data, function (idx, dom) {
            if (dom.type == '开间') {
                roomMean[0] = numFixed(dom.roomMean);
                riceMean[0] = numFixed(dom.riceMean);
            } else if (dom.type == '一居') {
                roomMean[1] = numFixed(dom.roomMean);
                riceMean[1] = numFixed(dom.riceMean);
            } else if (dom.type == '两居') {
                roomMean[2] = numFixed(dom.roomMean);
                riceMean[2] = numFixed(dom.riceMean);
            } else if (dom.type == '三居及以上') {
                roomMean[3] = numFixed(dom.roomMean);
                riceMean[3] = numFixed(dom.riceMean);
            }
        });
    }
    // 基于准备好的dom，初始化echarts实例
    var difbedroom= echarts.init(document.getElementById('different-bedroom'));
    // 指定图表的配置项和数据
    var option1 = {
        tooltip : {
            trigger: 'axis'
        },
        toolbox: {
            show : true,
            feature : {
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        legend: {
            data:['整租居室','单平租金']
        },
        xAxis : [
            {
                type : 'category',
                data : rooms
            }
        ],
        yAxis : [
            {
                type : 'value',
                name : '套均租金(元)',
                axisLabel : {
                    formatter: '{value} '
                }
            },
            {
                type : 'value',
                name : '单平租金(元/平米)',
                axisLabel : {
                    formatter: '{value} '
                }
            }
        ],
        series : [

            {
                name:'整租居室',
                type:'bar',
                data:roomMean
            },
            {
                name:'单平租金',
                type:'line',
                yAxisIndex: 1,
                data:riceMean
            }
        ],
        grid:{
            x:50,
            y:70,
            x2:50,
            y2:20
        }
    };
// 使用刚指定的配置项和数据显示图表。
    difbedroom.setOption(option1);
};

/*第二个图表（不同面积段租金）*/
function difArea(databack) {
    //console.log(databack.data);

    var rooms = ['10平以下','10-15','15-20','20-25','25-30','30-35','35-40','40-50','50-60','60-80','80-100','100-120','120及以上'];  //x轴刻度
    var len = rooms.length;
    var roomMean = new Array(len); //整套租金
    var riceMean = new Array(len);  //每平米租金
    for (var i=0;i<len;i++) {
        riceMean[i] = 0;
        roomMean[i] = 0;
    }
    if (databack.data != undefined) {
        // console.log(databack.data);
        $.each(databack.data, function (idx, dom) {
            if (dom.label == '10平以下') {
                roomMean[0] = numFixed(dom.roomMean);
                riceMean[0] = numFixed(dom.riceMean);
            } else if (dom.label == '10-15') {
                roomMean[1] = numFixed(dom.roomMean);
                riceMean[1] = numFixed(dom.riceMean);
            } else if (dom.label == '15-20') {
                roomMean[2] = numFixed(dom.roomMean);
                riceMean[2] = numFixed(dom.riceMean);
            } else if (dom.label == '20-25') {
                roomMean[3] = numFixed(dom.roomMean);
                riceMean[3] = numFixed(dom.riceMean);
            } else if (dom.label == '25-30') {
                roomMean[4] = numFixed(dom.roomMean);
                riceMean[4] = numFixed(dom.riceMean);
            } else if (dom.label == '30-35') {
                roomMean[5] = numFixed(dom.roomMean);
                riceMean[5] = numFixed(dom.riceMean);
            } else if (dom.label == '35-40') {
                roomMean[6] = numFixed(dom.roomMean);
                riceMean[6] = numFixed(dom.riceMean);
            } else if (dom.label == '40-50') {
                roomMean[7] = numFixed(dom.roomMean);
                riceMean[7] = numFixed(dom.riceMean);
            } else if (dom.label == '50-60') {
                roomMean[8] = numFixed(dom.roomMean);
                riceMean[8] = numFixed(dom.riceMean);
            } else if (dom.label == '60-80') {
                roomMean[9] = numFixed(dom.roomMean);
                riceMean[9] = numFixed(dom.riceMean);
            } else if (dom.label == '80-100') {
                roomMean[10] = numFixed(dom.roomMean);
                riceMean[10] = numFixed(dom.riceMean);
            } else if (dom.label == '100-120') {
                roomMean[11] = numFixed(dom.roomMean);
                riceMean[11] = numFixed(dom.riceMean);
            }else if (dom.label == '120及以上') {
                roomMean[12] = numFixed(dom.roomMean);
                riceMean[12] = numFixed(dom.riceMean);
            }
        });
    }

// 基于准备好的dom，初始化echarts实例
    var difarea = echarts.init(document.getElementById('different-area'));
// 指定图表的配置项和数据
    var option2 = {
        tooltip : {
            trigger: 'axis'
        },
        toolbox: {
            show : true,
            feature : {
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        legend: {
            data:['套(间)均租金','单平租金']
        },
        xAxis : [
            {
                type : 'category',
                data : rooms
            }
        ],
        yAxis : [
            {
                type : 'value',
                name : '套(间)均租金',
                axisLabel : {
                    formatter: '{value} '
                }
            },
            {
                type : 'value',
                name : '单平租金(元/平米)',
                axisLabel : {
                    formatter: '{value} '
                }
            }
        ],
        series : [

            {
                name:'套(间)均租金',
                type:'bar',
                data: roomMean
            },
            {
                name:'单平租金',
                type:'line',
                yAxisIndex: 1,
                data: riceMean
            }
        ],
        grid:{
            x:50,
            y:70,
            x2:50,
            y2:20
        }
    };

// 使用刚指定的配置项和数据显示图表。
    difarea.setOption(option2);
};

/*第三个图表（不同居室供应量）*/
function difAmount(databack) {
    // console.log(databack.data);
    var rooms = ['合租','开间','一居','两居','三居及以上'];  //x轴刻度
    var len = rooms.length;
    var quantity1 = new Array(len); //套数
    for (var i=0;i<len;i++) {
        quantity1[i] = 0;
    }
    if (databack.data != undefined) {
        // console.log(databack.data);
        $.each(databack.data, function (idx, dom) {
            if (dom.type == '合租') {
                quantity1[0] = numFixed(dom.quantity);
            } else if (dom.type == '开间') {
                quantity1[1] = numFixed(dom.quantity);
            } else if (dom.type == '一居') {
                quantity1[2] = numFixed(dom.quantity);
            } else if (dom.type == '两居') {
                quantity1[3] = numFixed(dom.quantity);
            } else if (dom.type == '三居及以上') {
                quantity1[4] = numFixed(dom.quantity);
            }
        });
    }
// 基于准备好的dom，初始化echarts实例
    var difamount = echarts.init(document.getElementById('different-amount'));

// 指定图表的配置项和数据
    var  option3 = {
        tooltip: {
            trigger: 'axis'
        },
        toolbox: {
            feature: {
                saveAsImage: {show: true}
            }
        },
        legend: {
            data:['挂牌套数','平均温度']
        },
        xAxis: [
            {
                type: 'category',
                data: rooms,
                axisPointer: {
                    type: 'shadow'
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '挂牌套数(套)',
                axisLabel: {
                    formatter: '{value} '
                }
            }
        ],
        series: [
            {
                name:'挂牌套数',
                type:'bar',
                data:quantity1
            }
        ],
        grid:{
            x:50,
            y:70,
            x2:50,
            y2:20
        }
    };

// 使用刚指定的配置项和数据显示图表。
    difamount.setOption(option3);
};

/*第四个图表（地铁房租金）*/
function difFilter(databack) {
    //console.log(databack.data);
    var typeSubway = ['开间','一居','两居','三居及以上'];  //x轴刻度
    var len = typeSubway.length;
    var Subway = new Array(len);  //地铁房
    var nearSubway = new Array(len); //近地铁房
    var notSubway = new Array(len);  //非地铁房
    var riceSubway = new Array(len); //地铁房单平租金
    var ricenearSubway = new Array(len); //近地铁房单平租金
    var ricenotSubway = new Array(len);  //非地铁房单平租金
    for (var i=0;i<len;i++) {
        Subway[i] = 0;
        nearSubway[i] = 0;
        notSubway[i] = 0;
        riceSubway[i] = 0;
        ricenearSubway[i] = 0;
        ricenotSubway[i] = 0;
    }
    if (databack.data != undefined) {
        // console.log(databack.data);
        $.each(databack.data, function (idx, dom) {
            if (dom.subway == 0) {
                if (dom.type == '开间') {
                    Subway[0] = numFixed(dom.roomMean);
                    riceSubway[0] = numFixed(dom.riceMean);
                } else if (dom.type == '一居') {
                    Subway[1] = numFixed(dom.roomMean);
                    riceSubway[1] = numFixed(dom.riceMean);
                } else if (dom.type == '两居') {
                    Subway[2] = numFixed(dom.roomMean);
                    riceSubway[2] = numFixed(dom.riceMean);
                } else if (dom.type == '三居及以上') {
                    Subway[3] = numFixed(dom.roomMean);
                    riceSubway[3] = numFixed(dom.riceMean);
                }
            } else if (dom.subway == 1) {
                if (dom.type == '开间') {
                    nearSubway[0] = numFixed(dom.roomMean);
                    ricenearSubway[0] = numFixed(dom.riceMean);
                } else if (dom.type == '一居') {
                    nearSubway[1] = numFixed(dom.roomMean);
                    ricenearSubway[1] = numFixed(dom.riceMean);
                } else if (dom.type == '两居') {
                    nearSubway[2] = numFixed(dom.roomMean);
                    ricenearSubway[2] = numFixed(dom.riceMean);
                } else if (dom.type == '三居及以上') {
                    nearSubway[3] = numFixed(dom.roomMean);
                    ricenearSubway[3] = numFixed(dom.riceMean);
                }
            } else if (dom.subway == 2) {
                if (dom.type == '开间') {
                    notSubway[0] = numFixed(dom.roomMean);
                    ricenotSubway[0] = numFixed(dom.riceMean);
                } else if (dom.type == '一居') {
                    notSubway[1] = numFixed(dom.roomMean);
                    ricenotSubway[1] = numFixed(dom.riceMean);
                } else if (dom.type == '两居') {
                    notSubway[2] = numFixed(dom.roomMean);
                    ricenotSubway[2] = numFixed(dom.riceMean);
                } else if (dom.type == '三居及以上') {
                    notSubway[3] = numFixed(dom.roomMean);
                    ricenotSubway[3] = numFixed(dom.riceMean);
                }
            }
        });
    }
    var diffilter = echarts.init(document.getElementById('different-filter'));

// 指定图表的配置项和数据
    var option4 = {
        tooltip: {
            trigger: 'axis'
        },
        toolbox: {
            feature: {
                saveAsImage: {show: true}
            }
        },
        legend: {
            orient: 'horizontal',
            padding: [10,30,10,30],
            data:['地铁房','近地铁房','非地铁房','地铁房单平平均租金','近地铁房单平平均租金','非地铁房单平平均租金']
        },
        xAxis: [
            {
                type: 'category',
                data: typeSubway,
                axisPointer: {
                    type: 'shadow'
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '套均租金(元)',
                axisLabel: {
                    formatter: '{value} '
                }
            },
            {
                type: 'value',
                name: '单平租金(元/平米)',
                axisLabel: {
                    formatter: '{value} '
                }
            }
        ],
        series: [
            {
                name:'地铁房',
                type:'bar',
                data: Subway
            },
            {
                name:'近地铁房',
                type:'bar',
                data: nearSubway
            },
            {
                name:'非地铁房',
                type:'bar',
                data: notSubway
            },
            {
                name:'地铁房单平平均租金',
                type:'line',
                yAxisIndex: 1,
                data: riceSubway
            },
            {
                name:'近地铁房单平平均租金',
                type:'line',
                yAxisIndex: 1,
                data: ricenearSubway
            },
            {
                name:'非地铁房单平平均租金',
                type:'line',
                yAxisIndex: 1,
                data: ricenotSubway
            }
        ],
        grid:{
            x:50,
            y:100,
            x2:50,
            y2:30
        }
    };

// 使用刚指定的配置项和数据显示图表。
    diffilter.setOption(option4);
}
